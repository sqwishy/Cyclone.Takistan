/* INEPT_fnc_rotatedVector
 *
 * Parameters:
 *  0: 2d/3d vector
 *  1: A number of degrees
 *
 * Specification:
 *  Returns argument 0 rotated around the z axis by argument 1 degrees.
 */

#include "common\defines.h"
#include "data.h"

params ["_vec", "_theta"];
if (_theta == 0) exitWith {_vec};
if (count _vec == 2) then
{
    [
        (_vec select 0) * (cos _theta) - (_vec select 1) * (sin _theta),
        (_vec select 0) * (sin _theta) + (_vec select 1) * (cos _theta)
    ]
}
else
{
    [
        (_vec select 0) * (cos _theta) - (_vec select 1) * (sin _theta),
        (_vec select 0) * (sin _theta) + (_vec select 1) * (cos _theta),
        _vec select 2
    ]
}
