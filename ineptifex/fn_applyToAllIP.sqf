/* INEPT_fnc_applyToAllIP
 *
 * Parameters:
 *  0: An array
 *  1: A callable
 *
 * Specification:
 *  Replaces each element in argument 0 with the result of calling argument 1
 *  where _x is the element being replaced. A function commonly called `map` in
 *  other languages.
 *
 * Example:
 *  _x = [3, 2, 1, 0];
 *  [_x, {_x ^ 2}] call INEPT_fnc_applyToAllIP;
 *  assert (_x isEqualTo [9, 4, 1, 0]);
 */

#define DEBUG_VAR "INEPT_fnc_applyToAllIP_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

{
    (ARGUMENT_0) set [_forEachIndex, call ARGUMENT_1];
}
forEach (ARGUMENT_0);
