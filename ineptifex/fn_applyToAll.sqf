/* INEPT_fnc_applyToAll
 *
 * Similar to INEPT_fnc_applyToAllIP but returns the result instead of
 * modifying the argument in place.
 */

#define DEBUG_VAR "INEPT_fnc_applyToAll_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_copy) = ARGUMENT_0 + [];
[_copy, ARGUMENT_1] call INEPT_fnc_applyToAllIP;
_copy
