/* INEPT_fnc_checkForDeadFolk
 *
 * Parameters:
 *  0: An AO instance
 *  1: A pawn instance
 *
 * Specification:
 *  Checks for dead bodies known about by the given pawn and correlates them
 *  with known contacts in order to confirm kills.
 *  This is called by INEPT_fnc_processPawnOnce, so you shouldn't have to call
 *  this directly.
 *
 * Requirements:
 *  The mutex in the PAWN_MUTEX field of the given pawn (argument 1) must be
 *  locked.
 */

#define DEBUG_VAR "INEPT_fnc_checkForDeadFolk_is_verbose"

#define KILL_CHECKED_PROP "INEPT_prop_checkedIfRecognizedCorpse"
#define KILL_CONFIRM_MAX_SEARCH_AHEAD  600
#define KILL_CONFIRM_MAX_SEARCH_BEHIND 80

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_ao) = ARGUMENT_0;
VAR(_pawn) = ARGUMENT_1;
VAR(_grp) = _pawn select PAWN_GROUP;

assert (MUTEX_IS_LOCKED(_pawn select PAWN_MUTEX));

MUTEX_LOCK(_ao select AO_CONTACTS_MUTEX);

VAR(_contacts) = _ao select AO_CONTACTS;

// Sure ... I mean ... why not ... right?
// But seriously, this is a pretty sad way of doing things. If it is possible
// to use arma's native unit detection bullshit, this would be much more
// straightforward and less expensive.
VAR(_is_low_visibility) = [getPosATL leader _grp] call INEPT_fnc_isPosUrban;

VAR(_near_stuff) = [
    position leader _grp,
    direction leader _grp,
    // Radius
    if (_is_low_visibility) then
        {
            (KILL_CONFIRM_MAX_SEARCH_AHEAD + KILL_CONFIRM_MAX_SEARCH_BEHIND) / 4
        }
        else
        {
            (KILL_CONFIRM_MAX_SEARCH_AHEAD + KILL_CONFIRM_MAX_SEARCH_BEHIND)
        },
    // Dist behind
    if (_is_low_visibility) then {KILL_CONFIRM_MAX_SEARCH_BEHIND / 4}
        else {KILL_CONFIRM_MAX_SEARCH_BEHIND},
    // Filter thing
    "AllVehicles"
    ] call INEPT_fnc_objectsInFrontOf;

{
    if (
        (not (_x getVariable [KILL_CHECKED_PROP, false]))
        and {if (_x isKindOf "Man") then
                {
                    not alive _x
                }
                else
                {
                    [_near_stuff, crew _x] call BIS_fnc_arrayPushStack;
                    // Is the damage thing really a good idea? What if folks just bail out
                    // of a perfectly undamaged vehicle?
                    ([crew _x, {not alive _x}] call INEPT_fnc_all) and {damage _x > 0}
                }
            }
        ) then
    {
        LOGVAR(_x);
        LOGVAR(typeOf _x);
        VAR(_pos) = getPosATL _x;
        LOGVAR(_pos);
        VAR(_correlation) = [
            _contacts,
            typeOf _x,
            _pos,
            time
            ] call INEPT_fnc_correlateContact;
        VAR(_contact_idx) = _correlation select PAIR_FIRST;
        if (_contact_idx != -1) then
        {
            LOGMSG("Guy died, correlated with contact, removing contact");
            LOGVAR(_correlation);
            VAR(_cargo_idx) = _correlation select PAIR_SECOND;
            if (_cargo_idx == -1) then
            {
                _contacts deleteAt _contact_idx;
            }
            else
            {
                (
                    (
                        (_contacts select _contact_idx) select CONTACT_INFO
                    )
                    select UNIT_INFO_CARGO
                )
                deleteAt _cargo_idx;
            };
        }
        else
        {
            LOGMSG("Guy died, but no correlated was made");
        };
        _x setVariable [KILL_CHECKED_PROP, true];
    };
}
forEach _near_stuff;

MUTEX_UNLOCK(_ao select AO_CONTACTS_MUTEX);
