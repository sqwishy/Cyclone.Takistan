/* INEPT_fnc_stealNearbyVehicle
 *
 * Parameters:
 *
 * Specification:
 *
 */

#define DEBUG_VAR "INEPT_fnc_stealNearbyVehicle_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_pawn) = ARGUMENT_0;
VAR(_grp) = _pawn select PAWN_GROUP;

// ._. this is crummy and I should stop doing it
VAR(_is_low_visibility) = [getPosATL leader _grp] call INEPT_fnc_isPosUrban;

VAR(_near_stuff) = [
    position leader _grp,
    direction leader _grp,
    // Radius
    if (_is_low_visibility) then {100} else {400},
    // Dist behind
    if (_is_low_visibility) then {20} else {60},
    // Filter thing
    "AllVehicles"
    ] call INEPT_fnc_objectsInFrontOf;

// todo, should we exclude certain classes? like SVDs? I mean, it's pretty
// hilarious to watch the AI jump inside beached SVDs and just sit there, but
// I can see how it might end up imparing their combat effectiveness

VAR(_stealable) = [];

{
    LOGVAR(_x);
    LOGVAR(crew _x);
    LOGVAR(canMove _x);
    LOGVAR(someAmmo _x);
    LOGVAR(fuel _x);
    LOGVAR(getPosASL _x);
    if (
        ((crew _x) isEqualTo [])
        and {canMove _x}
        // todo, check that the weapon is not damaged?
        and {someAmmo _x}
        and {fuel _x > (0.2 + random 0.2)}
        and {((getPosASL _x) select 2) > 0}
        and {(locked _x) in [0, 1]}
        ) exitWith
    {
        LOGMSG("Stealable");
        _stealable pushBack _x;
    };
}
forEach _near_stuff;

if !(_stealable isEqualTo []) then
{
    VAR(_steal_me_idx) = [
        _stealable,
        {-((leader _grp) distance _x)}
        ] call INEPT_fnc_indexOfBest;
    LOGVAR(_stealable);
    LOGVAR(_steal_me_idx);
    [units _grp, _stealable select _steal_me_idx] call INEPT_fnc_assignToVehicle;
};
