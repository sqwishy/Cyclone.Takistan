#include "data.h"

#define NEW_BEHAVIOR_STATE   []

#define DEFEND_WP_NAME  "__INEPT_wp_defend"
#define FLANK_WP_NAME   "__INEPT_wp_flank"
#define ASSAULT_WP_NAME "__INEPT_wp_assault"

#define NEW_DEFEND_BEHAVIOR_STATE()        [WP_TYPE_UNKNOWN]
#define DEFEND_BEHAVIOR_STATE_LAST_WP_TYPE 0
