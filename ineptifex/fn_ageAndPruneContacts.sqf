/* INEPT_fnc_ageAndPruneContacts
 *
 * Parameters:
 *  0: An AO instance
 *
 * Specification:
 *  Looks through recorded contacts in the given AO and removes them if they
 *  have become super old.
 *  This is called by INEPT_fnc_processAOOnce, so you don't normally need to
 *  call this.
 */


#define DEBUG_VAR "INEPT_fnc_ageAndPruneContacts_is_verbose"

#include "common\defines.h"
#include "data.h"

// Forget about stale contact reports after some number of seconds
#define CONTACT_TTL 12 * 60

LOGENTRY();

VAR(_ao) = ARGUMENT_0;

VAR(_removed) = [];

MUTEX_LOCK(_ao select AO_CONTACTS_MUTEX);

VAR(_contacts) = _ao select AO_CONTACTS;

{
    VAR(_expiry) = CONTACT_TTL + (_x select CONTACT_LASTSEEN);
    if (time > _expiry) then
    {
        LOGMSG("Found expired contact, removing");
        LOGVAR(_x);
        _contacts set [_forEachIndex, 0];
    };
}
forEach _contacts;

_ao set [AO_CONTACTS, _contacts - [0]];

MUTEX_UNLOCK(_ao select AO_CONTACTS_MUTEX);
