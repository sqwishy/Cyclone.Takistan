/* INEPT_fnc_getPawnPos
 *
 * Parameters:
 *  0: A pawn
 *
 * Specification:
 *  Returns a 3D vector indicating the AGL(?) position of the pawn.
 */

#define DEBUG_VAR "INEPT_fnc_getPawnPos_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_pawn) = ARGUMENT_0;
getPos leader (_pawn select PAWN_GROUP)
