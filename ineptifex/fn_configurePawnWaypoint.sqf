/* INEPT_fnc_configurePawnWaypoint
 *
 * Parameters:
 *  0: An AO instance
 *  1: An AOI instance
 *  2: A pawn instance
 *  3: A waypoint
 *  4: A waypoint type, see WP_TYPE_* in data.h
 *
 * Specification:
 *  Configures the passed waypoint, setting the position, type, completion
 *  radius, in accordance with the given waypoint type, aoi, and pawn.
 *  Will avoid setting the waypoint speed and formation, as those are
 *  controlled by the behaviour.
 *  Returns true if successful, false otherwise.
 */

#define DEBUG_VAR "INEPT_fnc_configurePawnWaypoint_is_verbose"

// For doing a flanking maneuver
#define FLANK_RADIUS    200

#include "common\defines.h"
#include "data.h"

LOGENTRY();

params ["_ao", "_aoi", "_pawn", "_wp", "_wp_type"];

VAR(_grp) = _pawn select PAWN_GROUP;
LOGVAR(_grp);

VAR(_did_anything) = false;

switch (_wp_type) do
{
case WP_TYPE_GARRISON:
{
    // Ensure we have units to garrison
    if ([units _grp, {
            (not ((vehicle _x) isEqualTo _x))
            and {not ((typeOf vehicle _x) isKindOf ["Car", configFile >> "CfgVehicles"])}
        }] call INEPT_fnc_any)
        exitWith {/* Don't garrison if we are in a group with a vehicle that isn't a car */};
    VAR(_garrison_me) = [
        units _grp,
        {vehicle _x == _x}
        ] call BIS_fnc_conditionalSelect;
    if (_garrison_me isEqualTo [])
        exitWith {/* Don't garrison if nobody is on foot? */};

    private _buildings = ([_pawn] call INEPT_fnc_getPawnPos) nearObjects ["Building", 75];
    LOGVAR(_buildings);
    private _idx =
        [
            _buildings,
            {
                // Something about the building having a path.
                if !((nearestBuilding _x) isEqualTo _x) exitWith {false};
                // Building must have at least 5 positions
                if ([0, 0, 0] isEqualTo (_x buildingPos 4)) exitWith {false};
                VAR(_garrison) = _x getVariable "__INEPT_garrison";
                // Building cannot be already occupied
                // this is a bit racey ...
                if (!isNil "_garrison"
                    && {!((waypointPosition _garrison) isEqualTo [0, 0, 0])}
                    && {(getPosATL _x) distance (waypointPosition _garrison) < 10}) exitWith {false};
                //if ([0, 0, 0] isEqualTo (_x buildingPos 4)) exitWith {false};
                true
            }
        ] call INEPT_fnc_indexOfFirstAtRandom;
    LOGVAR(_idx);

    // Just give up, life is not worth living
    if (_idx == -1) exitWith { LOGMSG("Couldn't find a nice building to garrison"); };

    VAR(_bld) = _buildings select _idx;
    VAR(_dest) = getPosATL _bld;
    [_garrison_me, 0.5 + random 0.5] call INEPT_fnc_selectFractionAtRandomIP;
    [_garrison_me, _bld] call INEPT_fnc_garrisonUnits;

    LOGVAR(_bld);
    LOGVAR(typeOf _bld);
    LOGVAR(_dest);
    _wp setWpPos _dest;
    _wp setWaypointType "MOVE";
    _wp setWaypointCompletionRadius 20;

    // TODO, publicVariable it so that other AOs are effected?
    // TODO, srsly, the above todo is kind of important
    _bld setVariable ["__INEPT_garrison", _wp];

    _did_anything = true;
};
case WP_TYPE_FLANK:
{
    // Find a vector pointing from the AOI to the pawn +/- several degrees
    // to either side
    VAR(_pawn_pos) = [_pawn] call INEPT_fnc_getPawnPos;
    VAR(_diff) = _pawn_pos vectorDiff (_aoi select AOI_POS);
    _diff set [2, 0];
    VAR(_dir) = vectorNormalized _diff;
    VAR(_theta) = 80 * ((random 1) ^ 1.4) * (if (random 1 >= 0.5) then {-1} else {1});
    LOGVAR(_theta);
    _dir = [_dir, _theta] call INEPT_fnc_rotatedVector;
    VAR(_dist) = FLANK_RADIUS + (_aoi select AOI_DISPERSION);
    VAR(_pos) = (_aoi select AOI_POS) vectorAdd (_dir vectorMultiply _dist);
    VAR(_area) = NEW_CIRCLE(_pos, FLANK_RADIUS);
    VAR(_subareas) = [_area, WP_SUBDIVISION_RADIUS] call INEPT_fnc_subdivide;
    // Only consider areas above sea level
    [_subareas, {getTerrainHeightASL _x > 0}] call INEPT_fnc_conditionalSelectIP;
    VAR(_dest) = if (_subareas isEqualTo []) then
        {
            _pos
        }
        else
        {
            // And throw some randomness into it just to make shit interesting
            [_subareas, 0.4] call INEPT_fnc_selectFractionAtRandomIP;
            _subareas select (
                [
                    // TODO, use terrainIntersect to find a spot that is in
                    // defilade and pick a spot with high elevation or
                    // something.
                    #define FLANK_LOOKAHEAD     30
                    #define FLANK_HEIGHT_DIFF   10
                    _subareas,
                    {
                        VAR(_fore_height) = (
                            getTerrainHeightASL
                            (
                                [_x select 0, _x select 1, 0]
                                vectorAdd
                                (_dir vectorMultiply (-FLANK_LOOKAHEAD))
                            )
                            );
                        1
                        /
                        (0.1 max (abs (
                            _fore_height - (getTerrainHeightASL _x) - FLANK_HEIGHT_DIFF
                            )))
                     }
                ] call INEPT_fnc_indexOfBest
                )
        };

    LOGVAR(_dest);
    _wp setWpPos _dest;
    _wp setWaypointType "MOVE";
    _wp setWaypointCompletionRadius 20;
    _did_anything = true;
};
case WP_TYPE_ASSAULT:
{
    VAR(_area) = NEW_CIRCLE(
        _aoi select AOI_POS,
        _aoi select AOI_DISPERSION
        );
    VAR(_subareas) = [_area, WP_SUBDIVISION_RADIUS] call INEPT_fnc_subdivide;
    // Only consider areas above sea level
    [_subareas, {getTerrainHeightASL _x > 0}] call INEPT_fnc_conditionalSelectIP;
    VAR(_dest) = if (_subareas isEqualTo []) then
        {
            _aoi select AOI_POS
        }
        else
        {
            [
                _subareas call INEPT_fnc_popRandom,
                random WP_SUBDIVISION_RADIUS
            ] call INEPT_fnc_deviatedVector
        };

    if ([units _grp, {vehicle _x == _x}] call INEPT_fnc_all) then
    {
        VAR(_sample) = selectBestPlaces [_dest, WP_SUBDIVISION_RADIUS, "forest + houses", 2, 1];
        if !(_sample isEqualTo []) then
        {
            _dest = (_sample select 0) select 0;
        };
    };

    LOGVAR(_dest);
    _wp setWpPos _dest;
    _wp setWaypointType "MOVE";
    _wp setWaypointCompletionRadius 20;
    _did_anything = true;
};
case WP_TYPE_OVERWATCH:
{
    // todo, some day replace with (_aoi select AOI_AREA)
    VAR(_area) = NEW_CIRCLE(
        _aoi select AOI_POS,
        _aoi select AOI_DISPERSION
        );

    private _num_samples = ceil (((_aoi select AOI_DISPERSION) ^ 0.70) / 8);
    private _samples = [];
    while {_num_samples = _num_samples - 1; _num_samples >= 0} do
    {
        _samples pushBack
            ([_aoi select AOI_POS, _aoi select AOI_DISPERSION]
                call INEPT_fnc_randomPointInCircle);
    };
    [_samples, {getTerrainHeightASL _x > 0}]
        call INEPT_fnc_conditionalSelectIP;
    LOGVAR(_samples);
    private _idx =
        [_samples, {
            private _asl_pos = [_x select 0, _x select 1, getTerrainHeightASL _x];
            private _quality = [_asl_pos] call INEPT_fnc_visibilityQuality;
            private _vision = 1 + ([_ao, _asl_pos, _pawn] call INEPT_fnc_hasVision);
            (_quality / _vision ^ 1.5)
        }] call INEPT_fnc_indexOfBest;

    LOGVAR(_idx);
    if (_idx == -1) exitWith {};

    private _dest = _samples select _idx;

    LOGVAR(_dest);
    _wp setWpPos _dest;
    _wp setWaypointType "MOVE";
    _wp setWaypointCompletionRadius 20;
    _did_anything = true;
};
default
{
    ["Unexpected value for waypoint type: %1", _wp_type] call BIS_fnc_error;
};
};

LOGVAR(getWPPos _wp);
_did_anything
