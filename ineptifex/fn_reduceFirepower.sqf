/* INEPT_fnc_reduceFirepower
 *
 * Parameters:
 *  0: Defending firepower
 *  1: Attacking firepower
 *
 * Specification:
 *  Returns a copy of the defending firepower, modified by an amount that
 *  resembles an estimation of how much damage the attacking firepower would.
 */

#define DEBUG_VAR "INEPT_fnc_reduceFirepower_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_def) = ARGUMENT_0;
VAR(_att) = ARGUMENT_1;

// Copy as to not modify the argument
_def = _def + [];

// This is some weird bullshit that I just made up, it doesn't make any sense,
// feel free to come up with something cleverer

VAR(_off_multiplier) = 1;
if ((_att select FIREPOWER_I) > 0) then
{
    VAR(_def_a) = .2 max (_def select FIREPOWER_AI);
    _off_multiplier = _off_multiplier * (_att select FIREPOWER_I) / _def_a;
};
if ((_att select FIREPOWER_R) > 0) then
{
    VAR(_def_a) = .2 max (_def select FIREPOWER_AR);
    _off_multiplier = _off_multiplier * (_att select FIREPOWER_R) / _def_a;
};
if ((_att select FIREPOWER_A) > 0) then
{
    VAR(_def_a) = .2 max (_def select FIREPOWER_AA);
    _off_multiplier = _off_multiplier * (_att select FIREPOWER_A) / _def_a;
};

//_off_multiplier = _off_multiplier ^ 2;

LOGVAR(_def);
LOGVAR(_att);
LOGVAR(_off_multiplier);

VAR(__adjust_fn) =
{
    VAR(_def_attr) = ARGUMENT_0;
    VAR(_att_attr) = ARGUMENT_1;
    _def set [
        _def_attr,
        0 max (
            (_def select _def_attr) -
                (_off_multiplier * (_att select _att_attr))
            )];
};

[FIREPOWER_AI, FIREPOWER_AI] call __adjust_fn;
[FIREPOWER_I,  FIREPOWER_AI] call __adjust_fn;
[FIREPOWER_AR, FIREPOWER_AR] call __adjust_fn;
[FIREPOWER_R,  FIREPOWER_AR] call __adjust_fn;
[FIREPOWER_AA, FIREPOWER_AA] call __adjust_fn;
[FIREPOWER_A,  FIREPOWER_AA] call __adjust_fn;

_def
