/* INEPT_fnc_selectFractionAtRandomIP
 *
 * Parameters:
 *  0: A list of thingies
 *  1: A scalar from 0 to 1
 *
 * Specification:
 *  Similar to INEPT_fnc_selectFractionAtRandom, but operates in-place, so that
 *  argument 0 is modified, no value is returned
 */

#define DEBUG_VAR "INEPT_fnc_selectFractionAtRandomIP_is_verbose"

#include "common\defines.h"

LOGENTRY();

VAR(_list) = ARGUMENT_0;
VAR(_pct) = ARGUMENT_1;
LOGVAR(_list);
LOGVAR(_pct);

VAR(_count) = ceil ((count _list) * _pct);
LOGVAR(_count);
if (_count == 0) exitWith
{
    _list deleteRange [0, count _list];
};
VAR(_to_rm) = (count _list) - _count;
LOGVAR(_to_rm);
while {_to_rm > 0} do
{
    _list call INEPT_fnc_popRandom;
    _to_rm = _to_rm - 1;
};
