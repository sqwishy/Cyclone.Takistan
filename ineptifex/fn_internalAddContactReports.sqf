/* INEPT_fnc_internalAddContactReports
 *
 * Does the heavy lifting for INEPT_fnc_addContactReports.
 *
 * Arguments:
 *   0: _current_contacts, array of contacts.
 *   1: _new_contacts, array of contacts?
 *
 * Goes through _new_contacts and tries to see if anything in there resembles
 * stuff from _current_contacts. If it does, it removes them so that replaces that
 * contact with the new one that correlated with it in _current_contacts, I
 * guess.
 * It also tries to handle cargo correlation, but it's anyone's guess what it
 * does or if it's correct.
 *
 * The first argument will almost certainly be modified, it will generally be
 * missing elements that were correlated with the second argument. The second
 * will not be modified ... I think.
 */

#define DEBUG_VAR "INEPT_fnc_internalAddContactReports_is_verbose"

#include "common\defines.h"
#include "data.h"

params ["_current_contacts", "_new_contacts"];

LOGENTRY();

{
    if (_current_contacts isEqualTo []) exitWith {};

    VAR(_new_contact) = _x;
    LOGVAR(_new_contact);
    VAR(_correlation) = [
        _current_contacts,
        (_new_contact select CONTACT_INFO) select UNIT_INFO_CLASS,
        _new_contact select CONTACT_POS,
        _new_contact select CONTACT_LASTSEEN,
        _new_contact select CONTACT_MOBILITY
        ] call INEPT_fnc_correlateContact;

    LOGVAR(_correlation);

    if (LOGGING_ENABLED()) then
    {
        LOGVAR(count _current_contacts);
        {
            diag_log (format [
                    "Contact: %1 (%2)",
                    (_x select CONTACT_INFO) select UNIT_INFO_CLASS,
                    (_x select CONTACT_POS)
                    ]);
            diag_log (
                [
                    (_x select CONTACT_INFO) select UNIT_INFO_CARGO,
                    {_x select UNIT_INFO_CLASS}
                ]
                call INEPT_fnc_applyToAll
                );
        }
        forEach _current_contacts;
    };

    VAR(_contact_idx) = _correlation select PAIR_FIRST;
    if (_contact_idx == -1) then
    {
        LOGMSG("No suitable contacts found for correlation");
        // Try to correlate cargo of new contact with our memory of past
        // contacts.
        VAR(_new_cargo) = (_new_contact select CONTACT_INFO) select UNIT_INFO_CARGO;
        if (not (_new_cargo isEqualTo [])) then
        {
            VAR(_new_cargo_contacts) =
                [ _new_cargo
                , {
                    NEW_CONTACT(_new_contact select CONTACT_POS,
                                _x,
                                _new_contact select CONTACT_LASTSEEN,
                                _new_contact select CONTACT_MOBILITY)
                  }
                ] call INEPT_fnc_applyToAll;
            [_current_contacts, _new_cargo_contacts] call INEPT_fnc_internalAddContactReports;
        };
    }
    else
    {
        LOGMSG("Similar contact found, removing");
        VAR(_cargo_idx) = _correlation select PAIR_SECOND;
        VAR(_correlated_info) =
            if (_cargo_idx == -1) then
            {
                VAR(_correlated_contact) = _current_contacts deleteAt _contact_idx;

                VAR(_old_cargo) = (_correlated_contact select CONTACT_INFO) select UNIT_INFO_CARGO;
                VAR(_new_cargo) = (_new_contact select CONTACT_INFO) select UNIT_INFO_CARGO;
                LOGVAR(_old_cargo);
                LOGVAR(_new_cargo);

                {
                    // We intentionally use the position of the _new_contact
                    // instead of the _correlated_contact for some reason.
                    _current_contacts pushBack
                        NEW_CONTACT(_new_contact select CONTACT_POS,
                                    _x,
                                    _correlated_contact select CONTACT_LASTSEEN,
                                    (_correlated_contact select CONTACT_MOBILITY) max ([_x select UNIT_INFO_CLASS] call INEPT_fnc_getMobility));
                }
                forEach _old_cargo;

                if (not (_new_cargo isEqualTo [])) then
                {
                    VAR(_new_cargo_contacts) =
                        [ _new_cargo
                        , {
                            NEW_CONTACT(_new_contact select CONTACT_POS,
                                        _x,
                                        _new_contact select CONTACT_LASTSEEN,
                                        _new_contact select CONTACT_MOBILITY)
                          }
                        ] call INEPT_fnc_applyToAll;

                    // When we add a new vehicle, we try to correlate its cargo
                    // with our memory of past contacts.
                    // Beware, all indexes we had for elements in
                    // _current_contacts are buggered after this point.
                    [_current_contacts, _new_cargo_contacts] call INEPT_fnc_internalAddContactReports;
                };

                _correlated_contact select CONTACT_INFO
            }
            else 
            {
                VAR(_correlated_contact) = _current_contacts select _contact_idx;
                LOGVAR(_correlated_contact);
                ((_correlated_contact select CONTACT_INFO) select UNIT_INFO_CARGO) deleteAt _cargo_idx
            };

        (_new_contact select CONTACT_INFO) set
            [ UNIT_INFO_EQUIPMENT
            , _correlated_info select UNIT_INFO_EQUIPMENT ];
    };
}
forEach _new_contacts;
