/* INEPT_fnc_objectsInFrontOf
 *
 * Parameters:
 *  0: Your mom
 *
 * Specification:
 *  Returns the result of nearObjects about the position in front of the
 *  origin (argument 0) such that the origin would be 'argument 3' metres
 *  inside a circle with a radius of 'argument 2' metres. The centre of the
 *  circle is 'argument 1' degrees from the origin, unless your 'argument 3' is
 *  greater than 'argument 2', in which case it's probably the reverse
 *  direction. This is a really bad explanation, but you can probably just read
 *  the source code if you really want to know what this does, honestly I
 *  should draw a picture because that would explain what this is supposed to
 *  do quite a bit better than some sort of undergraduate attempt at a formal
 *  mathematical definition.
 */

#define DEBUG_VAR "INEPT_fnc_objectsInFrontOf_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

LOGVAR(ARGUMENT);

VAR(_origin) = ARGUMENT_0;
VAR(_direction) = ARGUMENT_1;
VAR(_radius) = ARGUMENT_2;
VAR(_dist_behind) = ARGUMENT_3;
VAR(_filter) = ARGUMENT_4;

VAR(_search_pos) = (
    _origin
    vectorAdd
    (
     [
        [0, _radius - _dist_behind, 0],
        _direction
     ] call INEPT_fnc_rotatedVector
    )
    );
LOGVAR(_search_pos);

(_search_pos) nearObjects [_filter, _radius]
