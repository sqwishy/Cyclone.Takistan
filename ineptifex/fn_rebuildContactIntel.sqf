/* INEPT_fnc_rebuildContactIntel
 *
 * Parameters:
 *  0: An AO instance
 *
 * Specification:
 *  Uses pawns to get info on all of the baddies that it knows about and uses
 *  that to update its list of known contacts.
 *  This is called by INEPT_fnc_processAOOnce, so you don't normally need to
 *  call this.
 *  I imagine that stuff in here is incredibly slow and inefficient ... todo,
 *  more heuristics.
 */

#define DEBUG_VAR "INEPT_fnc_rebuildContactIntel_is_verbose"

// FIXME, duplicated
#define KILL_CHECKED_PROP "INEPT_prop_checkedIfRecognizedCorpse"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_ao) = ARGUMENT_0;

MUTEX_LOCK(_ao select AO_PAWNS_MUTEX);

VAR(_pawns) = _ao select AO_PAWNS;
if (_pawns isEqualTo []) exitWith
{
    MUTEX_UNLOCK(_ao select AO_PAWNS_MUTEX);
};

// todo, if a thing is unknown, it might be worth checking out
VAR(_enemy_sides) = [_ao select AO_SIDE] call BIS_fnc_enemySides;

// Determine what all the contacts are
VAR(_all_contacts) = [];
// Used to prevent duplicate entries (todo, not heuristic enough?)
VAR(_all_contacts_keys) = [];
// Keeps the greatest knowsAbout value for each contact
VAR(_all_contacts_knowledge) = [];
{
    VAR(_grp) = _x select PAWN_GROUP;
    // We want _all_contacts to be able to be considered a set, so make sure
    // additions to it preserve the uniqueness of its elements
    VAR(_tgts) = (leader _grp) nearTargets 1600;
    {
        VAR(_accuracy) = _x select 5;
        if (_accuracy < 10) then
        {
            VAR(_side) = _x select 2;
            if (_side in _enemy_sides) then
            {
                VAR(_obj) = _x select 4;
                VAR(_knows_aboot) = _grp knowsAbout _obj;
                VAR(_idx) = _all_contacts_keys find _obj;
                if (_idx == -1) then
                {
                    _all_contacts pushBack _x;
                    _all_contacts_keys pushBack _obj;
                    _all_contacts_knowledge pushBack _knows_aboot;
                }
                else
                {
                    if ((_all_contacts_knowledge select _idx) < _knows_aboot) then
                    {
                        _all_contacts_knowledge set [_idx, _knows_aboot];
                    };
                };
            };
        };
    }
    forEach _tgts;
}
forEach _pawns;
LOGVAR(_all_contacts);

if (missionNamespace getVariable ["INEPT_param_EnableContactReveal", true]) then
{
// This is an overly-complicated way of sharing knowledge with groups who are
// far away from the contact. Otherwise they get spooked over something that is
// none of their worry. Idealliy, they wouldn't but ...
#define DISTANCE_STOP 400
#define DISTANCE_DROPOFF 600
    {
        VAR(_grp) = _x select PAWN_GROUP;
        VAR(_grp_pos) = [_x] call INEPT_fnc_getPawnPos;
        {
            VAR(_contact_unit) = _x select 4;
            VAR(_knowledge) = _all_contacts_knowledge select _forEachIndex;
            VAR(_dist) = _contact_unit distance (_grp_pos);
            _knowledge = _knowledge * (1 - ((0 max ((_dist - DISTANCE_STOP) / DISTANCE_DROPOFF)) min 1));
            if ((_knowledge > 0) and {_knowledge > (_grp knowsAbout _contact_unit)}) then
            {
                LOGMSG("Elevated knowledge");
                LOGVAR(_grp);
                LOGVAR(_contact_unit);
                LOGVAR(_knowledge);
                _grp reveal [_contact_unit, _knowledge];
            };
        }
        forEach _all_contacts;
    }
    forEach _pawns;
};

// Use the same value for CONTACT_LASTSEEN in all new contacts we create here,
// even if we get suspended for a bit in the middle of execution
VAR(_current_time) = time;

VAR(_new_contacts) =
    [ _all_contacts
    , {
        VAR(_pos) = _x select 0;
        VAR(_unit) = _x select 4;
        // When we spot a dead thing, we mark it with this property so that we
        // don't confirm the same kill twice. We need to ensure the property is not
        // set here in case people re-enter a vehicle that was previously damaged
        // or a body becomes reanimated
        _unit setVariable [KILL_CHECKED_PROP, nil];
        [_unit, _pos, _current_time] call INEPT_fnc_contactFromUnit
      }
    ] call INEPT_fnc_applyToAll;

LOGVAR(_new_contacts);

[_ao, _new_contacts] call INEPT_fnc_addContactReports;

LOGVAR(_ao select AO_CONTACTS);

MUTEX_UNLOCK(_ao select AO_PAWNS_MUTEX);
