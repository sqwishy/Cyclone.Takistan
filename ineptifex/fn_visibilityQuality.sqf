/* INEPT_fnc_visibilityQuality
 *
 * Parameters:
 *  0: An ASL position
 *
 * Specification:
 *  Returns a scalar from 0 to 1, indicating the visibilty quality of that
 *  spot. 1 would denote that you can see quite well.
 *  NOTE: This ingores most stuff for now, it only checks if terrain is in the
 *  way, not buildings or anything else.
 *  This was done for performance reasons, and because heuristics are fun, but
 *  I'm on the fence about whether or not it should be changed.
 */

#define DEBUG_VAR "INEPT_fnc_visibilityQuality_is_verbose"

#include "common\defines.h"
#include "data.h"

#define ELEVATION   2

LOGENTRY();

params ["_origin"];

// This is kind of a hack, elevate by an amount so that if our given origin was
// underneath the terrain then we won't intersect it
_origin = _origin vectorAdd [0, 0, ELEVATION];

private _q = 0;
private _checks =
// >>> rotate_vec = lambda x, y, d: (x * math.cos(d) - y * math.sin(d), x * math.sin(d) + y * math.cos(d))
// >>> sum([[[round(x, 4), round(y, 4), 0] for (x,y) in [thingy(dist, 0, math.pi/180*d) for d in range(0,360,30)]] for dist in [350, 600, 1100, 1500]], [])
[[350.0, 0.0, 0], [303.1089, 175.0, 0], [175.0, 303.1089, 0], [0.0, 350.0, 0], [-175.0, 303.1089, 0], [-303.1089, 175.0, 0], [-350.0, 0.0, 0], [-303.1089, -175.0, 0], [-175.0, -303.1089, 0], [-0.0, -350.0, 0], [175.0, -303.1089, 0], [303.1089, -175.0, 0], [600.0, 0.0, 0], [519.6152, 300.0, 0], [300.0, 519.6152, 0], [0.0, 600.0, 0], [-300.0, 519.6152, 0], [-519.6152, 300.0, 0], [-600.0, 0.0, 0], [-519.6152, -300.0, 0], [-300.0, -519.6152, 0], [-0.0, -600.0, 0], [300.0, -519.6152, 0], [519.6152, -300.0, 0], [1100.0, 0.0, 0], [952.6279, 550.0, 0], [550.0, 952.6279, 0], [0.0, 1100.0, 0], [-550.0, 952.6279, 0], [-952.6279, 550.0, 0], [-1100.0, 0.0, 0], [-952.6279, -550.0, 0], [-550.0, -952.6279, 0], [-0.0, -1100.0, 0], [550.0, -952.6279, 0], [952.6279, -550.0, 0], [1500.0, 0.0, 0], [1299.0381, 750.0, 0], [750.0, 1299.0381, 0], [0.0, 1500.0, 0], [-750.0, 1299.0381, 0], [-1299.0381, 750.0, 0], [-1500.0, 0.0, 0], [-1299.0381, -750.0, 0], [-750.0, -1299.0381, 0], [-0.0, -1500.0, 0], [750.0, -1299.0381, 0], [1299.0381, -750.0, 0]]
;
private _num_checks = count _checks;

{
    private _dest = _origin vectorAdd _x;
    private _terrain_height = getTerrainHeightASL _dest;
    _dest set [2, (0 max _terrain_height) + ELEVATION];
    if !(terrainIntersectASL [_origin, _dest]) then
    {
        private _score = 1 / _num_checks;
        if (_terrain_height <= 0) then
        {
            // Water counts for less, we don't really need to watch it
            // because they would never attack by sea ...
            _score = _score * 0.3;
        };
        _q = _q + _score;
    };
}
forEach _checks;
_q
