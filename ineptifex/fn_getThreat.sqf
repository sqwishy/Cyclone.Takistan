/* INEPT_fnc_getThreat
 *
 * Parameters:
 *  0: A firepower
 *
 * Specification:
 *  Returns a scalar representing the threat of the given firepower, a larger
 *  values indicates more of a threat.
 */

#define DEBUG_VAR "INEPT_fnc_getThreat_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_fp) = ARGUMENT_0;

(
    ((_fp select FIREPOWER_I)
        + (_fp select FIREPOWER_R)
        + (_fp select FIREPOWER_A))
    * ((_fp select FIREPOWER_AI)
        + (_fp select FIREPOWER_AR)
        + (_fp select FIREPOWER_AA))
)
