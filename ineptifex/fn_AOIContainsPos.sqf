/* INEPT_fnc_AOIContainsPos
 *
 * Parameters:
 *  0: An AOI instance
 *  1: A position
 *
 * Specification:
 *  Returns true iff the given position is inside of the area covered by the
 *  given AOI.
 */

#define DEBUG_VAR "INEPT_fnc_AOIContainsPos_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_aoi) = ARGUMENT_0;
VAR(_pos) = ARGUMENT_1;

LOGMSG((_aoi select AOI_POS) distance _pos);
LOGMSG(_aoi select AOI_DISPERSION);

((_aoi select AOI_POS) distance _pos) <= (_aoi select AOI_DISPERSION)
