/* INEPT_fnc_prunePawns
 *
 * Parameters:
 *  0: An AO instance
 *
 * Specification:
 *  Removes dead/lame pawns from the given AO.
 *  This is called by INEPT_fnc_processAOOnce, so you don't normally need to
 *  call this.
 */

#define DEBUG_VAR "INEPT_fnc_prunePawns_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_ao) = ARGUMENT_0;
VAR(_mtx) = _ao select AO_PAWNS_MUTEX;
MUTEX_LOCK(_mtx);

VAR(_pawns) = _ao select AO_PAWNS;

[ _pawns
, {
    [ units (_x select PAWN_GROUP),
      // beware, the _x in the code block just below is different from the _x
      // above - because fuck clarity
      {alive _x}
    ] call INEPT_fnc_any;
  }
] call INEPT_fnc_conditionalSelectIP;

MUTEX_UNLOCK(_mtx);
