/* INEPT_fnc_removeAOI
 *
 * Parameters:
 *  0: An AO instance
 *  1: An AOI instance
 *
 * Specification:
 *  Removes the AOI to the AO. Returns true iff successful. False otherwise.
 */

#define DEBUG_VAR "INEPT_fnc_removeAOI_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_ao) = ARGUMENT_0;
VAR(_aoi) = ARGUMENT_1;

MUTEX_LOCK(_ao select AO_AOIS_MUTEX);

VAR(_r) =
    {
        if (_aoi isEqualTo _x) exitWith
        {
            (_ao select AO_AOIS) deleteAt _forEachIndex;
            true
        };
        false
    }
    forEach (_ao select AO_AOIS);

MUTEX_UNLOCK(_ao select AO_AOIS_MUTEX);
if (isNil "_r") exitWith {false};
