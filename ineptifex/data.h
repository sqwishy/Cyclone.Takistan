#include <common\defines.h>

#define BEHAVIOR_ASSAULT    0
#define BEHAVIOR_DEFEND     1

#define WP_TYPE_UNKNOWN         -1
#define WP_TYPE_FLANK           0
#define WP_TYPE_ASSAULT         1
#define WP_TYPE_GARRISON        2
#define WP_TYPE_OVERWATCH       3

#define UNIT_INFO_ACCURATE      0
#define UNIT_INFO_APPEARANCE    1
#define UNIT_INFO_WITH_CARGO    0
#define UNIT_INFO_NO_CARGO      1

#define NEW_EQUIPMENT()     [[], []]
#define EQUIPMENT_WEAPONS   0
#define EQUIPMENT_MAGAZINES 1

#define NEW_UNIT_INFO(classname)    [classname, NEW_EQUIPMENT(), []]
#define UNIT_INFO_CLASS     0
#define UNIT_INFO_EQUIPMENT 1
#define UNIT_INFO_CARGO     2

// Do we really need this?
#define EMPTY_SEAT_CLASS            "inept_empty_seat"
#define EMPTY_SEAT_INFO()           NEW_UNIT_INFO(EMPTY_SEAT_CLASS)

#define NEW_PAIR(first, second)     [first, second]
#define NULL_PAIR()         []
#define PAIR_FIRST          0
#define PAIR_SECOND         1

#define NEW_CONTACT(pos, info, thetime, mobility) \
    [pos, info, thetime, mobility]
#define NULL_CONTACT()      []
#define CONTACT_POS         0
#define CONTACT_INFO        1
#define CONTACT_LASTSEEN    2
#define CONTACT_MOBILITY    3

// todo, consider separating out the mobility and stealth thing into a
// different object
#define NEW_FIREPOWER()     [0, 0, 0, 0, 0, 0, -1, -1]
// Infantry, composition and lethality
#define FIREPOWER_I         0
#define FIREPOWER_AI        1
// Armour
#define FIREPOWER_R         2
#define FIREPOWER_AR        3
// Aircraft
#define FIREPOWER_A         4
#define FIREPOWER_AA        5
#define FIREPOWER_MOBILITY  6
#define FIREPOWER_STEALTH   7

#define GENERIC_INFANTRY_FIREPOWER() \
    [1, 1, 0, 0, 0, 0, 2.5, -1]

#define NEW_AOI(pos, disp, behave, is_reserve) \
    [pos, disp, NEW_FIREPOWER(), behave, is_reserve]
#define NULL_AOI()          []
#define AOI_POS             0
#define AOI_DISPERSION      1
#define AOI_FIREPOWER       2
#define AOI_BEHAVIOR        3
#define AOI_IS_RESERVE      4

#define NULL_WP()           []

// Radius used with INEPT_fnc_subdivide
#define WP_SUBDIVISION_RADIUS   25

// The behaviour states are implementation specific, so they are not defined
// here, only the null behaviour state is standard
#define NULL_BEHAVIOR_STATE()   []

#define NEW_BEHAVIOR_LOGIC(install_fn, tick_fn, uninstall_fn) \
    [install_fn, tick_fn, uninstall_fn]
#define BEHAVIOR_LOGIC_INSTALL      0
#define BEHAVIOR_LOGIC_TICK         1
#define BEHAVIOR_LOGIC_UNINSTALL    2

// Element might be a better name than pawn
#define NEW_PAWN(grp)       [grp, NULL_AOI(), NEW_MUTEX(), NULL_BEHAVIOR_STATE()]
#define NULL_PAWN()         NEW_PAWN(grpNull)
#define PAWN_GROUP          0
#define PAWN_AOI            1
#define PAWN_MUTEX          2
#define PAWN_BEHAVIOR_STATE 3

#define BIS_WP_GROUP        0
#define BIS_WP_INDEX        1

#define PAWN_WP_LEADING_NAME    "__INEPT_wp_leading"
#define PAWN_WP_PRE_NAME        "__INEPT_wp_pre"
#define PAWN_WP_TERM_NAME       "__INEPT_wp_term"

#define NEW_AREA(pos, width, height, angle, isrect) \
    [width, height, angle, isrect, pos]
#define NEW_CIRCLE(pos, radius) \
    NEW_AREA(pos, radius, radius, 0, false)
#define AREA_WIDTH          0
#define AREA_HEIGHT         1
#define AREA_ANGLE          2
#define AREA_ISRECT         3
// ... triggerArea won't return a list containing an element at index 4
#define AREA_POS            4

#define NEW_AO(side) \
    [side, [], NEW_MUTEX(), [], NEW_MUTEX(), [], NEW_MUTEX(), [], NEW_MUTEX()]
#define AO_SIDE                 0
#define AO_PAWNS                1
#define AO_PAWNS_MUTEX          2
#define AO_AOIS                 3
#define AO_AOIS_MUTEX           4
#define AO_CONTACTS             5
#define AO_CONTACTS_MUTEX       6
