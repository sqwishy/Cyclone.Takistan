/* INEPT_fnc_indexOfFirstAtRandom
 *
 * Parameters:
 *  0: An array of thingies
 *  1: A callable, returning true or false, where _x is an element in
 *     argument 0
 *
 * Specification:
 *  Calls argument 1 on elements in argument 0 at random, returns the first one
 *  that returns true. Returns -1 if it fails.
 */
#define DEBUG_VAR "INEPT_fnc_indexOfFirstAtRandom_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_list) = ARGUMENT_0;
if (_list isEqualTo []) exitWith {-1};
VAR(_indexes) = [];
{
    _indexes set [_forEachIndex, _forEachIndex];
}
forEach _list;
while {!(_indexes isEqualTo [])} do
{
    VAR(_idx) = _indexes call INEPT_fnc_popRandom;
    VAR(_x) = _list select _idx;
    if (call (ARGUMENT_1)) exitWith {_idx};
    -1
}
