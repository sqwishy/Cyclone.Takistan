/* INEPT_fnc_contactFromUnit
 *
 * Parameters:
 *  0: A unit object
 *  1: A 3d position ATL
 *  2: The time, like the return value of the `time` command
 *
 * Specification:
 *  Returns a new contact instance thing using argument 1 for the position, as
 *  opposed to getPosATL on argument 0.
 */

#define DEBUG_VAR "INEPT_fnc_contactFromUnit_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_unit) = ARGUMENT_0;
VAR(_pos) = ARGUMENT_1;
VAR(_time) = ARGUMENT_2;

VAR(_info) = [
    _unit,
    UNIT_INFO_APPEARANCE,
    UNIT_INFO_WITH_CARGO
    ] call INEPT_fnc_getUnitInfo;
VAR(_mobility) = [_info select UNIT_INFO_CLASS] call INEPT_fnc_getMobility;
NEW_CONTACT(_pos, _info, _time, _mobility)
