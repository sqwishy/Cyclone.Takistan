/* INEPT_fnc_isPosUrban
 *
 * Parameters:
 *  0: A position, probably a position ATL sort of thing
 *
 * Specification:
 *  Returns true or false depending on if the position is nearby a couple of
 *  buildings I guess.
 */

params ["_pos"];
private _cfg_vehicles = configFile >> "CfgVehicles";
// nearestBuilding check ensures that the thing really is very buildingy
(
    {
        ((nearestBuilding _x) isEqualTo _x) and
        {not ((getText (configFile >> typeOf _x >> "vehicleClass")) isEqualTo "Structures_Military")}
    }
    count (_pos nearObjects ["Building", 40])
)
>
3
