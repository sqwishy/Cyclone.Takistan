/* INEPT_fnc_triggerToArea
 *
 * Parameters:
 *  0: A trigger
 *
 * Specification:
 *  Returns an area from the trigger. It's like the triggerArea command but it
 *  also gets the trigger position and stores that in the AREA_POS field, see
 *  AREA_* in data.h
 */

#define DEBUG_VAR "INEPT_fnc_triggerToArea_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_trigger) = ARGUMENT_0;
VAR(_area) = triggerArea _trigger;
_area set [AREA_POS, getposATL _trigger];
_area
