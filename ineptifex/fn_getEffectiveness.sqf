/* INEPT_fnc_getEffectiveness
 *
 * Parameters:
 *  0: The defending firepower
 *  1: The attacking firepower
 *
 * Specification:
 *  Returns a floating point that indicates how effective we predict the
 *  assaulting force would be at damaging the defending force.
 *  Higher values mean it's better, I think. So 0 would be no effect, Infinity
 *  would be losses.
 */

#define DEBUG_VAR "INEPT_fnc_getEffectiveness_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_defending) = ARGUMENT_0;
VAR(_attacking) = ARGUMENT_1;

VAR(_att_threat) = [_attacking] call INEPT_fnc_getThreat;
// An attacking threat of zero? What is this, a world peace simulation?
if (_att_threat == 0) exitWith {0};
VAR(_def_threat) = [_defending] call INEPT_fnc_getThreat;
if (_def_threat == 0) exitWith {0};

_defending = [_defending, _attacking] call INEPT_fnc_reduceFirepower;
LOGVAR(_defending);

VAR(_new_def_threat) = [_defending] call INEPT_fnc_getThreat;
LOGVAR(_def_threat);
LOGVAR(_new_def_threat);
LOGVAR(_att_threat);

((_def_threat - _new_def_threat) ^ 3) / _att_threat
