/* INEPT_fnc_getFirepower
 *
 * Parameters:
 *  0: A unit info instance (see INEPT_fnc_getUnitInfo)
 *
 * Specification:
 *  Calculates and returns the firepower of the given unit info.
 *  todo, better handling of cases where the vehicle class name does not exist
 *  in CfgVehicles?
 */

#define DEBUG_VAR "INEPT_fnc_getFirepower_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_info) = ARGUMENT_0;

LOGVAR(_info);
VAR(_weps) = (_info select UNIT_INFO_EQUIPMENT) select EQUIPMENT_WEAPONS;
VAR(_mags) = (_info select UNIT_INFO_EQUIPMENT) select EQUIPMENT_MAGAZINES;
LOGVAR(_weps);
LOGVAR(_mags);

VAR(_class_name) = _info select UNIT_INFO_CLASS;

VAR(_is_infantry) = false;
VAR(_is_mechanized) = false;
VAR(_is_aircraft) = false;

VAR(_has_countermeasures) = false;

switch (true) do
{
    case (_class_name isKindOf "Man"):
    { _is_infantry = true };
    case (_class_name isKindOf "Car");
    case (_class_name isKindOf "Tank"):
    { _is_mechanized = true };
    case (_class_name isKindOf "Air"):
    { _is_aircraft = true };
};

////////////////////////////////////////////////////////////////////////////////
// Lethality

// Somewhere in here we try to evaluate how mean something is by determining if
// it can lock or not. In the past I found it difficult to find comprehensive
// and accurate information about locking and config file stuff.
//
// Helpful links:
// https://community.bistudio.com/wiki/A3_Locking_Review
// https://community.bistudio.com/wiki/CfgWeapons_Config_Reference
// https://community.bistudio.com/wiki/CfgMagazines_Config_Reference
// https://community.bistudio.com/wiki/CfgAmmo_Config_Reference
// https://community.bistudio.com/wiki/Arma_3_Damage_Description

// N.B.: Sometimes locking seems not to imply auto-seeking, in the case of the
// autocannon_35mm on the B_APC_Tracked_01_AA_F, it will simply set the range
// on the weapon

// TODO, take a hint from the threat attribute on vehicles? or the cost?
// We can't rely on those values entirely, we need to consider the ammo
// available so we know which weapons can be used.

// The following three variables are each a list of pairs which describe
// estimated potential lethality per second of weapons calculated below, as
// well as how many seconds it would take to use all of the available
// ammunition required for being that lethal.
// For example, one element in _lethality_i might be [1.4, 43], 1.4 lethality
// sustainable for 43 seconds, which might be the result of an mk200 with two
// boxes of ammunition.

VAR(_lethality_i) = [];
VAR(_lethality_r) = [];
VAR(_lethality_a) = [];

{
    VAR(_wep_cfg) = configFile >> "CfgWeapons" >> _x;
    VAR(_wep_canLock) = getNumber (_wep_cfg >> "canLock");
    VAR(_wep_muzzle_mags) = [_x] call INEPT_fnc_magazinesForWeaponCfg;

    LOGVAR(_wep_cfg);
    LOGVAR(_wep_canLock);

    {
        VAR(_muzzle_cfg) = _x select PAIR_FIRST;
        VAR(_muzzle_mags) = _x select PAIR_SECOND;
        LOGVAR(_muzzle_cfg);
        LOGVAR(_muzzle_mags);
        if (_muzzle_mags isEqualTo []) exitWith
        {
            // This sometimes happens if the weapon is a horn or binoculars or
            // something, but in that case, we should skip over that weapon
            // before we ever get here by looking at its weapon type or some
            // shit like that
            if (not isNil "INEPT_param_complainAboutMaglessWeapons") then
            {
                ["No magazines found for weapon %1 muzzle %2", _wep_cfg, _muzzle_cfg] call BIS_fnc_error;
            };
        };

        // Separate mode configs where showToPlayer is true
        VAR(_player_mode_cfg) = [];
        VAR(_ai_mode_cfg) = [];
        {
            VAR(_x_mode_cfg) = if (_x == "this") then
                {_wep_cfg}
                else
                {_wep_cfg >> _x};
            if ((getNumber (_x_mode_cfg >> "showToPlayer")) == 1) then
            {
                _player_mode_cfg pushBack _x_mode_cfg;
            }
            else
            {
                _ai_mode_cfg pushBack _x_mode_cfg;
            };
        }
        forEach (getArray (_muzzle_cfg >> "modes"));

        LOGVAR(_player_mode_cfg);
        LOGVAR(_ai_mode_cfg);

        if (_player_mode_cfg isEqualTo []) exitWith
        {
            ["No showToPlayer modes found for weapon %1 muzzle %2", _wep_cfg, _muzzle_cfg] call BIS_fnc_error;
        };

        if (_ai_mode_cfg isEqualTo []) then
        {
            // This is pretty common for launchers or aircraft weapons.
            //["No non showToPlayer modes found for weapon %1 muzzle %2 - using showToPlayer ones instead", _wep_cfg, _muzzle_cfg] call BIS_fnc_error;
            _ai_mode_cfg = _player_mode_cfg;
        };

        // Everything is going splendidly so far, we should have a muzzle and
        // one of its modes.
        // Now add entries for figure out how much firepower we can dispense
        // with the magazines we have.

        // TODO, check optics.
        VAR(_interval_at_1400) = 0;
        VAR(_reloadTime) = 0;
        {
            _reloadTime = _reloadTime + (getNumber (_x >> "reloadTime"));
            VAR(_interval) = (getNumber (_x >> "aiRateOfFire")) max (getNumber (_x >> "reloadTime"));
            VAR(_distance) = getNumber (_x >> "aiRateOfFireDistance");
            VAR(_burst) = getNumber (_x >> "burst");
            _interval_at_1400 = _interval_at_1400 + (_interval / _burst) * (1400 / _distance);
        }
        forEach _ai_mode_cfg;
        // Obtain the averages
        _interval_at_1400 = _interval_at_1400 / (count _ai_mode_cfg);
        _reloadTime = _reloadTime / (count _ai_mode_cfg);
        LOGVAR(_interval_at_1400);
        LOGVAR(_reloadTime);

        // todo, figure out how to determine this reliably
        VAR(_magazineReloadTime) = 3.5;
        LOGVAR(_magazineReloadTime);

        VAR(_cmImmunity) = (getNumber (_wep_cfg >> "cmImmunity"));
        LOGVAR(_cmImmunity);

        {
            VAR(_all_mag_count) = count _mags;
            _mags = _mags - [_x];
            VAR(_num_mags_found) = _all_mag_count - count _mags;

            VAR(_mag_cfg) = nil;
            VAR(_ammo_cfg) = nil;
            VAR(_hit) = nil;

            if (
                _num_mags_found > 0
                and
                    {
                        _mag_cfg = configFile >> "CfgMagazines" >> _x;
                        _ammo_cfg = configFile >> "CfgAmmo" >> getText (_mag_cfg >> "ammo");
                        _hit = (getNumber (_ammo_cfg >> "hit"));
                        // TODO, improve this stuff
                        if !(_has_countermeasures) then
                        {
                            _has_countermeasures =
                                (_hit < 5) and {(getNumber (_ammo_cfg >> "weaponLockSystem")) > 0};
                        };
                        // If the hit value for this ammunition is too low,
                        // it's probably a smoke grenade or a flare or a
                        // countermeasure or something silly like that.
                        _hit > 5
                    }
                ) then
            {
                LOGVAR(_mag_cfg);
                LOGVAR(_ammo_cfg);

                VAR(_mag_size) = getNumber (_mag_cfg >> "count");
                VAR(_rounds_avail) = (_mag_size * _num_mags_found);
                LOGVAR(_mag_size);
                LOGVAR(_rounds_avail);

                VAR(_ammo_cost) = getNumber (_ammo_cfg >> "cost");
                VAR(_airLock) = getNumber (_ammo_cfg >> "airLock");
                VAR(_allowAgainstInfantry) =
                    if (isNumber (_ammo_cfg >> "allowAgainstInfantry")) then
                    {
                        getNumber (_ammo_cfg >> "allowAgainstInfantry")
                    }
                    else
                    {
                        1
                    };
                //VAR(_explosive) =        (getNumber (_ammo_cfg >> "explosive"));
                VAR(_indirectHit) =      (getNumber (_ammo_cfg >> "indirectHit"));
                VAR(_indirectHitRange) = (getNumber (_ammo_cfg >> "indirectHitRange"));
                VAR(_irLock) =           (getNumber (_ammo_cfg >> "irLock"));

                LOGVAR(_ammo_cost);
                LOGVAR(_airLock);
                LOGVAR(_allowAgainstInfantry);
                //LOGVAR(_explosive);
                LOGVAR(_hit);
                LOGVAR(_indirectHit);
                LOGVAR(_indirectHitRange);
                LOGVAR(_irLock);

                // Anti-Infantry Effectiveness

                if (_allowAgainstInfantry > 0) then
                {
                    // TODO, incorporate explosiveness
                    VAR(_round_damage_i) =
                        ((1 min (_hit / 20)) + (0.2 * (3 * _indirectHitRange)^2 * pi * (1 min (_indirectHit / 20))));
                    LOGVAR(_round_damage_i);

                    // /4 because infantry combat is at about 350 metres or some shit I guess
                    VAR(_interval_i) = (_reloadTime min (_interval_at_1400 / 4)) + (_magazineReloadTime / _mag_size);
                    LOGVAR(_interval_i);

                    // This is really just
                    // 0.7 * shots per second * damage per shot / cost per
                    // shot; where shots in one second is 1 / seconds interval
                    // between shots.
                    VAR(_this_lethality_i) = 0.7 * _round_damage_i / _interval_i / (1 max _ammo_cost);
                    LOGVAR(_this_lethality_i);

                    if (_this_lethality_i > 0) then
                    {
                        VAR(_sustainability_i) = _rounds_avail * _interval_i;
                        LOGVAR(_sustainability_i);
                        _lethality_i pushBack (NEW_PAIR(_this_lethality_i, _sustainability_i));
                    };
                };

                // Tank/Armor/Car Damage

                VAR(_can_at_lock) = (_wep_canLock >= 2) and (_irLock > 0) and (_airLock <= 1);
                LOGVAR(_can_at_lock);

                VAR(_round_damage_r) = ((((_hit min 180) / 180) ^ 4) + (((_hit - 180) max 0) / 20));
                LOGVAR(_round_damage_r);

                // / 2 because armour combat is at about 700 metres or some shit I guess
                VAR(_interval_r) = (_reloadTime min (_interval_at_1400 / 2)) + (_magazineReloadTime / _mag_size);
                LOGVAR(_interval_r);

                // This is mainly just
                // shots per second * damage per shot / cost per shot; where
                // shots in one second is 1 / seconds interval between shots.
                VAR(_accuracy_r) = if (_can_at_lock) then {_cmImmunity max .6} else {.6};
                VAR(_this_lethality_r) = _accuracy_r * (40 min _round_damage_r) / _interval_r / (1 max ((sqrt _ammo_cost) / 50));
                LOGVAR(_this_lethality_r);

                if (_this_lethality_r > 0) then
                {
                    VAR(_sustainability_r) = _rounds_avail * _interval_r;
                    LOGVAR(_sustainability_r);
                    _lethality_r pushBack (NEW_PAIR(_this_lethality_r, _sustainability_r));
                };

                // Air Damage
                // I guess, being effective against air is done by locking missiles
                // or by spamming an autocannon or something. So if we can't lock
                // with this ammo, take a pentalty based on our rate of fire and
                // size of magazine.

                VAR(_can_aa_lock) = (_wep_canLock >= 2) and (_irLock > 0) and (_airLock >= 1);
                LOGVAR(_can_aa_lock);

                VAR(_round_damage_a) = ((((_hit min 40) / 40) ^ 4) + (((_hit - 40) max 0) / 20));
                LOGVAR(_round_damage_a);

                // because air combat is at about 1400 metres or some shit I guess
                VAR(_interval_a) = (_reloadTime min (_interval_at_1400)) + (_magazineReloadTime / _mag_size);
                LOGVAR(_interval_a);

                // This is mainly just
                // shots per second * damage per shot / cost per shot; where
                // shots in one second is 1 / seconds interval between shots.
                VAR(_accuracy_a) = if (_can_aa_lock) then {_cmImmunity max .01} else {.01};
                VAR(_this_lethality_a) = _accuracy_a * (40 min _round_damage_a) / _interval_a / (1 max (_ammo_cost / 10000));
                LOGVAR(_this_lethality_a);

                if (_this_lethality_a > 0) then
                {
                    VAR(_sustainability_a) = _rounds_avail * _interval_a;
                    LOGVAR(_sustainability_a);
                    _lethality_a pushBack (NEW_PAIR(_this_lethality_a, _sustainability_a));
                };
            };
        }
        forEach _muzzle_mags;
    }
    forEach _wep_muzzle_mags;
}
forEach _weps;

LOGVAR(_lethality_i);
LOGVAR(_lethality_r);
LOGVAR(_lethality_a);

VAR(__sort_fn) = {_x select PAIR_FIRST};

VAR(_sorted_lethality_i) = [_lethality_i, nil, __sort_fn] call BIS_fnc_sortBy;
VAR(_sorted_lethality_r) = [_lethality_r, nil, __sort_fn] call BIS_fnc_sortBy;
VAR(_sorted_lethality_a) = [_lethality_a, nil, __sort_fn] call BIS_fnc_sortBy;

LOGVAR(_sorted_lethality_i);
LOGVAR(_sorted_lethality_r);
LOGVAR(_sorted_lethality_a);

// todo, put this in its own file or something for performance and tidyness and
// all that crap
VAR(__lethality_fn) =
{
    params ["_sorted", "_max"];
    VAR(_remaining) = _max;
    VAR(_r) = 0;
    while {!(_sorted isEqualTo [])} do
    {
        VAR(_pair) = _sorted deleteAt (count _sorted - 1);
        VAR(_lethality) = _pair select PAIR_FIRST;
        VAR(_duration) = _pair select PAIR_SECOND;
        if (_duration > _remaining) exitWith
        {
            _r = _r + _lethality * (_remaining / _max);
        };
        _r = _r + _lethality * (_duration / _max);
        _remaining = _remaining - _duration;
    };
    _r
};

// Reuse these variable names because I'm too lazy to pick proper ones, this is
// now the number values we will use for the firepower at the end.
// The numbers provided as the second argument are how many seconds of
// sustained lethatlity we care about, I just made these values up, they aren't
// super special.
// The result of the call ends up being a value representing something like how
// much of that sort of damage we can do every second.
// For the purpose of balancing that value with the constitution, we scale the
// result up. TODO, I'm not sure that the scaling matters. Except maybe for
// thingies created by the priorty value ... maybe ethat should be changed ...
// XXX FIXME TODO
_lethality_i = ([_sorted_lethality_i, 40] call __lethality_fn);
_lethality_r = ([_sorted_lethality_r, 15] call __lethality_fn);
_lethality_a = ([_sorted_lethality_a, 10] call __lethality_fn);

LOGVAR(_lethality_i);
LOGVAR(_lethality_r);
LOGVAR(_lethality_a);

////////////////////////////////////////////////////////////////////////////////
// Constitution - composition and defense and stuff

LOGVAR(_has_countermeasures);

VAR(_constitution_i) = 0;
VAR(_constitution_r) = 0;
VAR(_constitution_a) = 0;

switch (true) do
{
    // TODO, incorporate size as it effects accuracy of attackers
    case (_is_infantry):
    {
        // todo, check out the uniform or vest for armor values?
        _constitution_i = 1;
        // ... people could totally have countermeasures ...
        if (_has_countermeasures) then
        {
            _constitution_i = _constitution_i * 2;
        };
    };
    case (_is_mechanized):
    {
        VAR(_armor) = getNumber (configFile >> "CfgVehicles" >> _class_name >> "armor");
        _constitution_r = _armor / 100;
        if (_has_countermeasures) then
        {
            _constitution_r = _constitution_r * 1.5;
        };
    };
    case (_is_aircraft):
    {
        // Shouldn't we check for flairs? Maybe they aren't part of the unit
        // info. I thought they were a weapon ...
        VAR(_armor) = getNumber (configFile >> "CfgVehicles" >> _class_name >> "armor");
        _constitution_a = _armor / 40;
        if (_has_countermeasures) then
        {
            _constitution_a = _constitution_a * 2;
        };
    };
    default
    {
        // Should we do something here?
        ["%1 is not recognized, constitution uncertain.", _class_name] call BIS_fnc_error;
        _constitution_i = 0.5;
        _constitution_r = 0.5;
        _constitution_a = 0.5;
    };
};

////////////////////////////////////////////////////////////////////////////////
// And all the other bullshit

VAR(_speedyness) = [_class_name] call INEPT_fnc_getMobility;
VAR(_camoflakldfj) = getNumber (configFile >> "CfgVehicles" >> _class_name >> "camouflage");
_camoflakldfj = _camoflakldfj max .001;

VAR(_fp) = NEW_FIREPOWER();
_fp set [FIREPOWER_AI, _lethality_i];
_fp set [FIREPOWER_AR, _lethality_r];
_fp set [FIREPOWER_AA, _lethality_a];
_fp set [FIREPOWER_I, _constitution_i];
_fp set [FIREPOWER_R, _constitution_r];
_fp set [FIREPOWER_A, _constitution_a];
_fp set [FIREPOWER_MOBILITY, _speedyness];
_fp set [FIREPOWER_STEALTH, 1 / _camoflakldfj];
_fp
