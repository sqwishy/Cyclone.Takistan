/* INEPT_fnc_areaFromLocation */

#define DEBUG_VAR "INEPT_fnc_areaFromLocation_is_verbose"

#include "data.h"
#include "common\defines.h"

params ["_location"];
VAR(_size) = size _location;
VAR(_width) = 2 * (_size select 0);
VAR(_height) = 2 * (_size select 1);
NEW_AREA(position _location,
         _width,
         _height,
         direction _location,
         rectangular _location)
