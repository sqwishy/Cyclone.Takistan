/* INEPT_fnc_averageNumber
 *
 * There's no way there isn't a BIS function or something already for this, but
 * I can't find it ...
 */

#include "common\defines.h"

VAR(_sum) = 0;
{
    _sum = _sum + _x;
}
forEach _this;
if (count _this > 0) exitWith { _sum / count _this };
_sum
