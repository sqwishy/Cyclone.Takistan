/* INEPT_fnc_generateAOIs
 *
 * Parameters:
 *  0: An AO instance
 *
 * Specification:
 *  Returns all the AOIs that were added to the given AO with addAOI, as well
 *  as those produced by contact reports.
 */

#define DEBUG_VAR "INEPT_fnc_generateAOIs_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_ao) = ARGUMENT_0;
LOGVAR(_ao);

MUTEX_LOCK(_ao select AO_AOIS_MUTEX);
VAR(_aois) = _ao select AO_AOIS;
VAR(_r) = _aois + ([_ao] call INEPT_fnc_contactAOIs);
MUTEX_UNLOCK(_ao select AO_AOIS_MUTEX);
_r
