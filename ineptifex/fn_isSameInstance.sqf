/* INEPT_fnc_isSameInstance
 *
 * Parameters:
 *  0: An array
 *  1: Another array
 *
 * Specification:
 *  Returns true if argument 0 is the same array as argument 1. By same I mean
 *  actually the same thing, stronger than equality, literally the same thing.
 *  This works by modifying one array and seeing if the other one is modified
 *  in the same way. It's really a silly hack. I don't know why there isn't a
 *  built-in command that does this.
 */

#define DEBUG_VAR "INEPT_fnc_isSameInstance_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_lh) = ARGUMENT_0;
VAR(_rh) = ARGUMENT_1;
VAR(_count_lh) = count _lh;
VAR(_count_rh) = count _rh;
if (_count_lh != _count_rh) exitWith {false};
_lh pushBack 0;
VAR(_is_same) = (count _rh == (_count_rh + 1));
_lh resize (_count_lh);
_is_same
