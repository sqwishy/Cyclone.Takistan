// See INEPT_fnc_buildBehaviorLogics
// I wonder if this should take a behaviour as an argument instead of an AOI?

#define DEBUG_VAR "INEPT_fnc_getBehaviorLogic_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_aoi) = ARGUMENT_0;
LOGVAR(_aoi);

if (_aoi isEqualTo NULL_AOI()) then
    {
        INEPT_behaviorNull
    }
    else
    {
        VAR(_behavior) = _aoi select AOI_BEHAVIOR;
        LOGVAR(_behavior);
        switch (_behavior) do
        {
        case BEHAVIOR_ASSAULT:
        {
            INEPT_behaviorAssault
        };
        case BEHAVIOR_DEFEND:
        {
            INEPT_behaviorDefend
        };
        default
        {
            ["Unexpected value for AOI behavior: %1", _behavior] call BIS_fnc_error;
            INEPT_behaviorNull
        };
        }
    }
