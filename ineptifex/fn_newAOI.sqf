/* INEPT_fnc_newAOI
 *
 * Parameters:
 *  0: An area with an ATL position
 *  1: An array of length 3 defining the priority [infantry, armour, aircraft]
 *  2: Boolean stating if this is a reserve AOI
 *  3: The behaviour, see BEHAVIOR_* in data.h
 *
 * Specification:
 *  Creates a new AOI, does not add it to an AO, use INEPT_fnc_addAOI for that.
 *  When pawns are delegated, some may remain unassigned if all the AOIs
 *  already have enough pawns assigned to them. These leftover pawns will do
 *  nothing unless there are reserve AOIs, in which case they will be assigned
 *  to one of the reserve AOIs.
 */

#define DEBUG_VAR "INEPT_fnc_newAOI_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_area) = ARGUMENT_0;
VAR(_priority) = ARGUMENT_1;
VAR(_is_reserve) = ARGUMENT_2;
VAR(_behavior) = ARGUMENT_3;

if (
    (_area select AREA_ISRECT)
    or {!((_area select AREA_WIDTH) isEqualTo (_area select AREA_WIDTH))}
    ) exitWith
{
    ["Only circular shapes are supported for the moment"] call BIS_fnc_error;
    NULL_AOI()
};

VAR(_aoi) = NEW_AOI(
    _area select AREA_POS,
    _area select AREA_WIDTH,
    _behavior,
    _is_reserve
    );

LOGVAR(_priority);

VAR(_firepower) = NEW_FIREPOWER();
_firepower set [FIREPOWER_AI, _priority select 0];
_firepower set [FIREPOWER_I,  _priority select 0];
_firepower set [FIREPOWER_AR, _priority select 1];
_firepower set [FIREPOWER_R,  _priority select 1];
_firepower set [FIREPOWER_AA, _priority select 2];
_firepower set [FIREPOWER_A,  _priority select 2];
_aoi set [AOI_FIREPOWER, _firepower];

LOGVAR(_aoi);
_aoi
