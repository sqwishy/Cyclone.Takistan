/* INEPT_fnc_newAO
 *
 * Parameters:
 *  0: A side
 *
 * Specification:
 *  This is an important function! It returns a new AO instance for the given
 *  side.
 */

#define DEBUG_VAR "INEPT_fnc_newAO_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_ao) = NEW_AO(ARGUMENT_0);
LOGVAR(_ao);
_ao
