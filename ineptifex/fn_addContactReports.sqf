/* INEPT_fnc_addContactReports
 *
 * Parameters:
 *  0: An AO instance
 *  1: A list of contact instances
 *
 * Specification:
 *  Correlates argument 1 with current contacts and updates where applicable.
 *  Appends remaining contacts.
 *
 * Both arguments may be modified, so code passing values to this forfeits
 * ownership of those values.
 */

#define DEBUG_VAR "INEPT_fnc_addContactReports_is_verbose"

#include "common\defines.h"
#include "data.h"

LOGENTRY();

VAR(_ao) = ARGUMENT_0;
VAR(_new_contacts) = ARGUMENT_1;

LOGVAR(_new_contacts);

MUTEX_LOCK(_ao select AO_CONTACTS_MUTEX);

VAR(_current_contacts) = _ao select AO_CONTACTS;
LOGVAR(count _current_contacts);

[_current_contacts, _new_contacts] call INEPT_fnc_internalAddContactReports;

{
    LOGMSG("Appending new contact");
    LOGVAR(_x);
    _current_contacts pushBack _x;
}
forEach _new_contacts;

if (LOGGING_ENABLED()) then
{
    LOGVAR(count _current_contacts);
    {
        diag_log (format [
                "Contact: %1 (%2)",
                (_x select CONTACT_INFO) select UNIT_INFO_CLASS,
                (_x select CONTACT_POS)
                ]);
        diag_log (
            [
                (_x select CONTACT_INFO) select UNIT_INFO_CARGO,
                {_x select UNIT_INFO_CLASS}
            ]
            call INEPT_fnc_applyToAll
            );
    }
    forEach _current_contacts;
};

MUTEX_UNLOCK(_ao select AO_CONTACTS_MUTEX);
