// Arguments
//   _pos: 2d/3d pos, origin of circle
//   _rad: circle radius
// Returns:
//   Position of random point in the circle described by the parameters,
//   position has same dimension as the _pos parameter, only the x and y
//   elements are modified (probably).

params ["_pos", "_rad"];

// https://stackoverflow.com/questions/5837572/generate-a-random-point-within-a-circle-uniformly
private _u = random 1 + random 1;
private _r = if (_u > 1) then { 2 - _u } else { _u };
private _t = pi * 2 * random 1;
private "_final_pos";
if (count _pos > 2) then
{
    private _randvec = [_r * cos (180 * _t), _r * sin (180 * _t), _pos select 2];
    _final_pos = _pos vectorAdd (_randvec vectorMultiply _rad);
    _final_pos resize 2;
}
else
{
    private _randvec = [_r * cos (180 * _t), _r * sin (180 * _t), 0];
    _final_pos = (_pos + [0]) vectorAdd (_randvec vectorMultiply _rad);
    _final_pos resize 2;
};
_final_pos
