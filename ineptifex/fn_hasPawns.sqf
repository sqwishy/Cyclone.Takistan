/* INEPT_fnc_prunePawns
 *
 * Parameters:
 *  0: An AO instance
 *
 * Specification:
 *  Returns true iff the AO has any pawns/groups. Pawns with are removed by
 *  INEPT_fnc_processAOOnce when everyone in them is dead. So when forces
 *  belonging to an AO have been wiped out this should return false.
 */

#include "data.h"

params ["_ao"];
not ((_ao select AO_PAWNS) isEqualTo [])
