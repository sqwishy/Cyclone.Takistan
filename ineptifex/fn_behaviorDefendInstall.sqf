// See INEPT_fnc_buildBehaviorLogics

#define DEBUG_VAR "INEPT_fnc_behaviorDefendInstall_is_verbose"

#include "common\defines.h"
#include "data.h"
#include "behaviordata.h"

LOGENTRY();

params ["_ao", "_pawn"];
_pawn set [PAWN_BEHAVIOR_STATE, NEW_DEFEND_BEHAVIOR_STATE()];
private _grp = _pawn select PAWN_GROUP;
(leader _grp) setBehaviour "SAFE";
