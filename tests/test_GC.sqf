#include "..\cyclone\common\defines.h"

#include "base.sqf"

private _cy_test_GC_threads =
{
    private _loc = createLocation ["Strategic", [0, 1, 2], 100, 200];
    [__LINE__, {_loc}] call __assertIsNotNull;
    [[_loc]] call cy_fnc_GC_cycle;
    [__LINE__, {_loc}] call __assertIsNull;
};

private _cy_test_GC_groups =
{
	player setPosATL [0, 0, 0];
	private _grp = createGroup west;
	private _unit_a = _grp createUnit ["b_soldier_universal_f", [0, 0, 0], [], 0, "FORM"];
	_unit_a setPosATL [0, 0, 1000];
	private _unit_b = _grp createUnit ["b_soldier_universal_f", [0, 0, 0], [], 0, "FORM"];
	_unit_b setPosATL [0, 0, 1300];

	private _q = [_grp];

    [_q] call cy_fnc_GC_cycle;
    [__LINE__, {_grp}] call __assertIsNotNull;
    [__LINE__, {alive _unit_a}, true] call __assertIsEqual;
    [__LINE__, {alive _unit_b}, false] call __assertIsEqual;
	[__LINE__, {_q}, [_grp]] call __assertIsEqual;

	_unit_a setPosATL [0, 0, 2000];

    [_q] call cy_fnc_GC_cycle;
    [__LINE__, {_grp}] call __assertIsNull;
    [__LINE__, {alive _unit_a}, false] call __assertIsEqual;
    [__LINE__, {alive _unit_b}, false] call __assertIsEqual;
	[__LINE__, {_q}, []] call __assertIsEqual;
};

try
{
    LOGMSG("Running...");
    call _cy_test_GC_threads;
    call _cy_test_GC_groups;
    LOGMSG("All passed?");
}
catch
{
    LOGMSG(_exception);
};
