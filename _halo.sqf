#include "cyclone\common\defines.h"

halo_fnc_setup_object =
{
    _this addAction [ "HALO from map marker ..."
                    , halo_fnc_user_wants_to_halo
                    , []    /* arguments */
                    , 1     /* priority */
                    , false /* showWindow */ ];
};

halo_fnc_open_chute =
{
    var(_unit) = argument_0;
    _unit removeAction (_this select 2);
    var(_pos) = getPosATL _unit;
	var(_vel) = velocity _unit;
	var(_dir) = direction _unit;

    var(_para) = createVehicle ["Steerable_Parachute_F", _pos, [], 0, "FLY"];
	_unit moveInDriver _para;

    _para setPosATL _pos;
	_para setDir _dir;
	_para lock false;
    _para setVelocity _vel;
    _para
};

halo_fnc_do_halo =
{
    var(_unit) = argument_0;
    var(_pos) = argument_1;

    [ 0 // Out
    , "BLACK"
    , 0.4 // Duration
    , 1 // Blur
    ] spawn BIS_fnc_fadeEffect;

    sleep 0.5;

    _unit setPosATL _pos;

    [ 1 // In
    , "BLACK"
    , 0.4 // Duration
    , 1 // Blur
    ] spawn BIS_fnc_fadeEffect;

    _unit addAction [ "Open Chute"
                    , halo_fnc_open_chute
                    , [_unit]
                    , 1     // priority
                    , false // showWindow
                    , true  // hide on use
                    , "Eject"];
};

halo_fnc_at_position =
{
    var(_pos) = _this;
    [ player
    , [ _pos vectorAdd [0, 0, 400 + random 150]
      , random 40
      ] call INEPT_fnc_deviatedVector
    ] spawn halo_fnc_do_halo;
};

halo_fnc_user_wants_to_halo =
{
    var(_r) = call halo_fnc_prompt_halo_spot;
    if (not _r) then
    {
        hint "No markers found containing the text ""HALO"".";
    };
};

// good god this is fucking autistic
halo_fnc_paginate =
{
    params ["_dataset", "_fn_data_item", "_fn_more_item"];
    _dataset = [] + _dataset;
    var(_r) = [];
    var(_remaining) = 0;
    while {_remaining = count _dataset; _remaining > 0} do
    {
        if (_remaining <= 13) exitWith
        {
            var(_page) = [];
            {
                _page pushBack ([_x] call _fn_data_item);
            }
            forEach _dataset;
            _r pushBack _page;
        };

        var(_page) = [];
        var(_i) = 11 min _remaining;
        while {_i = _i - 1; _i >= 0} do
        {
            _page pushBack ([_dataset deleteAt 0] call _fn_data_item);
        };

        if (count _dataset > 0) then
        {
            _page append ([count _r + 1] call _fn_more_item);
        };

        _r pushBack _page;
    };
    _r
};

// holy shit what a disaster
halo_fnc_prompt_halo_spot =
{
    var(_pages) =
        [ [allMapMarkers, {toLower markerText _x find "halo" >= 0}] call BIS_fnc_conditionalSelect
        , {
              var(_txt) = markerText argument_0;
              var(_pos) = markerPos argument_0;
              var(_enabled) =
                  [[cy_player_thrall] call cy_fnc_Thrall_allPlots
                  ,{[cy_player_thrall, _x, cy_plot_fnc_isAllowedQuickEntryAtPos, [_pos], true] call cy_fnc_Thrall_inquirePlot}
                  ] call inept_fnc_all;
              var(_menu_text) = format ["[%2] %1 %3", _txt, mapGridPosition _pos, if (_enabled) then {""} else {"(Too close to an AO)"}];
              // Use the marker position instead of the marker name because we
              // have to pass it as a string and it could contain a " so
              // formatting it is kind of a pain.
              var(_expression) = format ["%1 call halo_fnc_at_position", _pos];
              [_menu_text, [0], "", -5, [["expression", _expression]], "1", if (_enabled) then {"1"} else {"0"}, ""]
          }
        , {
              var(_menu_name) = format ["#USER:halo_data_location_menu_%1", argument_0];
              [ ["", [0], "", -1, [], "1", "0", ""]
              , ["More...", [0], _menu_name, -5, [], "1", "1", ""] ]
          }
        ] call halo_fnc_paginate;
    {
        var(_thing) = format ["halo_data_location_menu_%1", _forEachIndex];
        missionNamespace setVariable [_thing, [["HALO Marker", true]] + _x];
    }
    forEach _pages;

    if (count _pages > 0) then
    {
        showCommandingMenu "#USER:halo_data_location_menu_0";
        true
    }
    else
    {
        false
    }
};
