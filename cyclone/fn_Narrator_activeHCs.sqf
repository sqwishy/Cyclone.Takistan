#include "common\defines.h"

params ["_narrator"];

// This doesn't really make sense to run if you aren't the server
assert isServer;

[ [_narrator select cy_data_Narrator_cyclone] call cy_fnc_Cyclone_getHCs
, {not local _x}
] call BIS_fnc_conditionalSelect
