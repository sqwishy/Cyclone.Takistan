params ["_thrall", "_plot", "_pos"];

private _plotlet = [_thrall, _plot] call cy_fnc_Thrall_getPlotData;
if (isNil {_plotlet}) then
{
    //["The plotlet variable is unset on this machine - this function won't work as you expect"] call BIS_fnc_error;
    false
}
else
{
    (_pos distance (_plotlet select cy_data_OwieKnee_task_pos)) < cy_var_OwieKnee_area_use_radius
}
