#include "common\defines.h"

params ["_narrator_var", "_plot", "_thingies"];

if !(_thingies isEqualType []) then
{
	_thingies = [_thingies];
};

LOGVAR(_this);
private _narrator = missionNamespace getVariable _narrator_var;
private _cyclone = _narrator select cy_data_Narrator_cyclone;
private _plot_gc_var = [_cyclone, "gc", _plot] call cy_fnc_Cyclone_plotVar;
if (isNil _plot_gc_var) then
{
	["Failed to add %1 to plot %2 GC - no gc queue for that plot was found. Adding it to Narrator GC queue instead."] call bis_fnc_error;
	(_narrator_var select cy_data_Narrator_gc_queue) append _thingies;
}
else
{
	(missionNamespace getVariable _plot_gc_var) append _thingies;
};
