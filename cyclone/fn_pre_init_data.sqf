#include "platform.sqf"

cy_var_is_ace_enabled = isClass (configfile >> "CfgPatches" >> "ace_common");

cy_var_suggested_tick_interval = 4;

cy_data_Counter      = DATA();
cy_data_Counter_init = pl_data_init;
cy_data_Counter_n    = ATTR(cy_data_Counter);
cy_data_Counter_min  = ATTR(cy_data_Counter);
cy_data_Counter_max  = ATTR(cy_data_Counter);

cy_data_Cyclone                 = DATA();
cy_data_Cyclone_init            = pl_data_init;
cy_data_Cyclone_server_logic    = ATTR(cy_data_Cyclone);
cy_data_Cyclone_hc_slots        = ATTR(cy_data_Cyclone);
cy_data_Cyclone_remote_exec_id  = ATTR(cy_data_Cyclone);
cy_data_Cyclone_drivers         = ATTR(cy_data_Cyclone);
cy_data_Cyclone_magic_prefix    = ATTR(cy_data_Cyclone);
cy_data_Cyclone_is_narrator     = ATTR(cy_data_Cyclone);
cy_data_Cyclone_is_player       = ATTR(cy_data_Cyclone);
cy_data_Cyclone_is_AI           = ATTR(cy_data_Cyclone);

cy_data_Narrator                = DATA();
cy_data_Narrator_init           = pl_data_init;
cy_data_Narrator_cyclone        = ATTR(cy_data_Narrator);
cy_data_Narrator_audiance_var   = ATTR(cy_data_Narrator);
cy_data_Narrator_counter        = ATTR(cy_data_Narrator);
cy_data_Narrator_plots_mutex    = ATTR(cy_data_Narrator);
cy_data_Narrator_plots          = ATTR(cy_data_Narrator);
cy_data_Narrator_gc_queue       = ATTR(cy_data_Narrator);

cy_var_Narrator_tick_interval = 3;

cy_data_Audiance            = DATA();
cy_data_Audiance_init       = pl_data_init;
cy_data_Audiance_mutex      = ATTR(cy_data_Audiance);
cy_data_Audiance_cyclone    = ATTR(cy_data_Audiance);
cy_data_Audiance_members    = ATTR(cy_data_Audiance);
cy_data_Audiance_last_value = ATTR(cy_data_Audiance);

cy_data_Thrall               = DATA();
cy_data_Thrall_init          = pl_data_init;
cy_data_Thrall_cyclone       = ATTR(cy_data_Thrall);
cy_data_Thrall_plot_role     = ATTR(cy_data_Thrall);
cy_data_Thrall_check_hc_slot = ATTR(cy_data_Thrall);
cy_data_Thrall_plots         = ATTR(cy_data_Thrall);
cy_data_Thrall_plots_mutex   = ATTR(cy_data_Thrall);
cy_data_Thrall_narrator_var  = ATTR(cy_data_Thrall);

cy_data_Map      = DATA();
cy_data_Map_init = pl_data_init;
cy_data_Map_size = ATTR(cy_data_Map); // In metres I guess

// Prefix (paird with plot id) for variable on a group
cy_var_Depot_group_claim_var = "cy_depot_claimed";
cy_var_Depot_forces_restored = "cy_depot_restored";

cy_data_Depot               = DATA();
cy_data_Depot_init          = pl_data_init;
cy_data_Depot_narrator_var  = ATTR(cy_data_Depot);
cy_data_Depot_items         = ATTR(cy_data_Depot);

cy_data_DepotItem           = DATA();
cy_data_DepotItem_init      = pl_data_init;
cy_data_DepotItem_source    = ATTR(cy_data_DepotItem);
cy_data_DepotItem_infos     = ATTR(cy_data_DepotItem);
cy_data_DepotItem_firepower = ATTR(cy_data_DepotItem);
cy_data_DepotItem_cost      = ATTR(cy_data_DepotItem);

// Used to heuristicly determine when a thread (that we don't have a handle
// for) or any code path has stopped doing its thing.
cy_data_Heartbeat          = DATA();
cy_data_Heartbeat_init     = pl_data_init;
cy_data_Heartbeat_intLimit = ATTR(cy_data_Heartbeat)
cy_data_Heartbeat_isDead   = ATTR(cy_data_Heartbeat)
cy_data_Heartbeat_lastBeat = ATTR(cy_data_Heartbeat)

// Plots are bits of information that refer to who can run it and what to run;
// Each occurrence is sent over the network to convey what current plots are
// active.
cy_data_Plot                 = DATA();
cy_data_Plot_init            = pl_data_init;
cy_data_Plot_id              = ATTR(cy_data_Plot);
cy_data_Plot_driver_serial   = ATTR(cy_data_Plot);
cy_data_Plot_hc_slot         = ATTR(cy_data_Plot);

cy_var_plot_suggested_hb_lifetime = 20;

cy_data_PlotDriver             = DATA();
cy_data_PlotDriver_mapping     = ATTR(cy_data_PlotDriver);
cy_data_PlotDriver_serial      = ATTR(cy_data_PlotDriver);
cy_data_PlotDriver_weight      = ATTR(cy_data_PlotDriver);
cy_data_PlotDriver_server_args = ATTR(cy_data_PlotDriver);
cy_data_PlotDriver_player_args = ATTR(cy_data_PlotDriver);
cy_data_PlotDriver_ai_args     = ATTR(cy_data_PlotDriver);

cy_var_PlotRole_server = 0;
cy_var_PlotRole_player = 1;
cy_var_PlotRole_ai     = 2;

// yuck ...
cy_plot_fnc_isPosInUse = 3;
cy_plot_fnc_isAllowedQuickEntryAtPos = 4;

// todo, maybe migrate ineptifex to this new style thing if this isn't
// completely shit
inept_data_AoI_pos        = 0;
inept_data_AoI_dispersion = 1;
inept_data_AoI_firepower  = 2;

// INEPT stuff

inept_data_Area        = DATA();
inept_data_Area_init   = pl_data_init;
// radius, and position are the only arguments in that order
inept_data_Area_init_circle =
{
    [_this select 0, _this select 0, 0, false, _this select 1]
};
inept_data_Area_width  = ATTR(inept_data_Area);
inept_data_Area_height = ATTR(inept_data_Area);
inept_data_Area_angle  = ATTR(inept_data_Area);
inept_data_Area_isrect = ATTR(inept_data_Area);
inept_data_Area_pos    = ATTR(inept_data_Area);

inept_data_UnitInfo           = DATA();
inept_data_UnitInfo_init      = pl_data_init;
inept_data_UnitInfo_class     = ATTR(inept_data_UnitInfo)
inept_data_UnitInfo_equipment = ATTR(inept_data_UnitInfo)
inept_data_UnitInfo_cargo     = ATTR(inept_data_UnitInfo)

inept_data_Firepower_zero = { [0, 0, 0, 0, 0, 0, -1, -1] };
inept_data_Firepower_init =
{
    if (count _this < 7) then
    {
        _this set [6, -1];
        _this set [7, -1];
    };
    _this
};
