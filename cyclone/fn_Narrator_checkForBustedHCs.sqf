#include "common\defines.h"

// Don't run this unless HCs are enabled and it is multiplayer

params ["_narrator"];

private _plots = [_narrator] call cy_fnc_Narrator_allPlots;

{
	private _hc_slot = _x select cy_data_Plot_hc_slot;
	if ((local _hc_slot) and {not isNull _hc_slot}) then
	{
		[_narrator, _x] call cy_fnc_Narrator_cancelPlot;
		LOGMSG("Cancelled busted plot");
		LOGVAR(_plot);
	};
}
forEach _plots;
