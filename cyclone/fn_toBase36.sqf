// Arguments
//   _int: A positive decimal integer.
// Returns
//   A string of the base 36 encoded value of the given integer.
// Example
//   12975 call cy_fnc_toBase36 -> "A0F"
// Disclaimer
//   This doesn't seem to work reliably for largish (greater than
//   999,999) numbers. Presumably because arma does some weird jank where
//   integers are actually floating points.

#include "common\defines.h"

// toArray "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
#define SAMPLE \
    [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90]

params ["_int"];

if (_int <= 0) exitWith { "0" };

var(_r) = [];

while {_int > 0} do
{
    var(_remain) = _int mod 36;
    _int = floor (_int / 36);
    _r pushBack (SAMPLE select _remain);
};

// this reverse is yuck ...
reverse _r;
toString _r
