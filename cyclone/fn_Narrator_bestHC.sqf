// Selects the least busy headless client

#include "common\defines.h"

params ["_narrator"];

var(_limit) =
    if (isMultiplayer and cy_param_WithHeadlessClients)
    then {cy_param_HeadlessClientAILimit}
    else {cy_param_GlobalAILimit};

([_narrator] call cy_fnc_Narrator_leastBusyHC) params ["_tgt", "_usage"];

if ((not isNull _tgt) and (_usage < _limit)) then
{
    _tgt
}
else
{
    objNull
}
