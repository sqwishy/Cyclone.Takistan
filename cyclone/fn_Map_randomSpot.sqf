// Arguments
//  0: Map instance thing (cy_data_Map)

#include "common\defines.h"

var(_size) = argument_0 select cy_data_Map_size;
var(_width) = _size select 0;
var(_height) = _size select 1;
[random _width, random _height]
