// cy_fnc_Depot_spawnUnits

#include "common\defines.h"

LOGENTRY();

params ["_depot", "_area", "_side", "_units", "_skill_profile", ["_key", ""]];

LOGVAR(_units);

private _positions = [_area, 50] call INEPT_fnc_subdivide;

private _pos = _area select inept_data_Area_pos;
private _pos_idx = [ _positions
                , {(getTerrainHeightASL _x) > 0}
                ] call INEPT_fnc_indexOfFirstAtRandom;

if (_pos_idx != -1) then
{
    _pos = _positions select _pos_idx;
};

private _grp =
   [ _pos  //_this select 0: the group's starting position (Array)
   , _side //_this select 1: the group's side (Side)
   , _units //_this select 2: can be three different types:
            // - list of character types (Array)
            // - amount of characters (Number)
            // - CfgGroups entry (Config)
   , nil //_this select 3: (optional) list of relative positions (Array)
   , nil //_this select 4: (optional) list of ranks (Array)
   , nil //_this select 5: (optional) skill range (Array)
   , nil //_this select 6: (optional) ammunition count range (Array)
   , nil //_this select 7: (optional) randomization controls (Array)
         //                [ amount of mandatory units (Number),
         //                  spawn chance for the remaining units (Number) ]
   , nil //_this select 8: (optional) azimuth (Number)
   , false //_this select 9: (optional) force precise position (Bool, default: true).
   , nil //_this select 10: (optional) max. number of vehicles (Number, default: 10e10).
   ] call BIS_fnc_spawnGroup;

{
    private _unit = _x;
    {
        _x params ["_attr", "_base", "_random"];
        _unit setSKill [_attr, _base + random _random];
    }
    forEach _skill_profile;
}
forEach (units _grp);

// fixme
zoos addCuratorEditableObjects [units _grp, true];

_grp
