#include "common\defines.h"

var(_hb) = argument_0;

if (_hb select cy_data_Heartbeat_isDead) then
{
    true
}
else
{
    if ((time - (_hb select cy_data_Heartbeat_lastBeat))
        > (_hb select cy_data_Heartbeat_intLimit)) then
    {
        _hb set [cy_data_Heartbeat_isDead, true];
        true
    }
    else
    {
        false
    }
}
