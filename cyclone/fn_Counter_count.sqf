// Arguments
//   _counter: Counter object thingy. (cy_data_Counter)

#include "common\defines.h"

params ["_counter"];
var(_n) = _counter select cy_data_Counter_n;
if ((_n + 1) > (_counter select cy_data_Counter_max)) then
{
    _counter set [cy_data_Counter_n, _counter select cy_data_Counter_min];
}
else
{
    _counter set [cy_data_Counter_n, _n + 1];
};
_n
