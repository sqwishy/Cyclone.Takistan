params ["_thrall", "_plot", "_key"];

private _var = [_thrall, _plot select cy_data_Plot_id] call cy_fnc_Thrall_plotletVar;
if !(isNil "_key") then
{
    _var = [_var, _key] joinString "_";
};

//#define SIMULATE_MP
#ifdef SIMULATE_MP
[] +
#endif
(missionNamespace getVariable _var)
