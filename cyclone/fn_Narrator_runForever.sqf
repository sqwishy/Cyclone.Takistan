#include "common\defines.h"

params ["_narrator"];

private _cyclone = _narrator select cy_data_Narrator_cyclone;
private _drivers = _cyclone select cy_data_Cyclone_drivers;

private _last_init = time - 20;

while {true} do
{
    private _hc_slot = nil;

    waitUntil
    {
        sleep cy_var_Narrator_tick_interval;

        [_narrator select cy_data_Narrator_gc_queue] call cy_fnc_GC_cycle;
        if (isMultiplayer and cy_param_WithHeadlessClients) then
        {
            // Marks plots and cancelled and removes them when the HC disconnected.
            [_narrator] call cy_fnc_Narrator_checkForBustedHCs;
        };
        // TODO FIXME, east should not be used here
        private _baddie_count = {(side _x) isEqualTo east} count allUnits;

        (_baddie_count < cy_param_GlobalAILimit)
        and {count ([_narrator] call cy_fnc_Narrator_allPlots) < cy_param_PlotCountLimit}
        and {_hc_slot = [_narrator] call cy_fnc_Narrator_bestHC; not isNull _hc_slot}
        and {count _drivers > 0}
        and {time - _last_init > 20}
    };

    private _driver_index =
        [ [_drivers, {_x select cy_data_PlotDriver_weight}] call INEPT_fnc_applyToAll
        ] call cy_fnc_weightedRandom;

    [ _narrator
    , _drivers select _driver_index
    , _hc_slot
    ] call cy_fnc_Narrator_Plot_init;

    _last_init = time;
};
