// cy_fnc_Depot_spawnForces
//
// Requirements:
//  ...

#define DEBUG_VAR "cy_fnc_Depot_spawnForces_isVerbose"

#include "common\defines.h"

LOGENTRY();

params ["_depot", "_area", "_side", "_firepower", "_skill_profile", ["_key", ""]];

LOGVAR(_depot);
LOGVAR(_area);
LOGVAR(_side);
LOGVAR(_firepower);

var(_all_items) = _depot select cy_data_Depot_items;
LOGVAR(_all_items);

assert (count _all_items > 0);

var(_selection) = [];
var(_cum_fp) = call inept_data_Firepower_zero;
var(_badness) = 0;
var(_cur_eff) = 0;
var(_failures) = 0;

// Stop spawning after:
//  - We feel we've provided enough firepower.
//  - We've spawned too many things.
//  - We failed too many times to find something good to spawn.
while
{
    _badness = [[_firepower, _cum_fp] call INEPT_fnc_reduceFirepower] call INEPT_fnc_getThreat;
    LOGVAR(_badness);
    _cur_eff = [_firepower, _cum_fp] call INEPT_fnc_getEffectiveness;
    LOGVAR(_cur_eff);
    (_badness > 0) && {_failures < 4} && {count _selection < 30}
}
do
{
    var(_some_items) = [_all_items, 0.6] call INEPT_fnc_selectFractionAtRandom;
    var(_effects) =
        [ _some_items
        , {
            var(_x_fp) = _x select cy_data_DepotItem_firepower;
            var(_try_fp) = [_cum_fp, _x_fp] call INEPT_fnc_addFirepower;
            ([_firepower, _try_fp] call INEPT_fnc_getEffectiveness) / (_x select cy_data_DepotItem_cost)
          }
        ] call INEPT_fnc_applyToAll;

    [ _effects
    , {
        // Filter out really bad decisions/ineffective choices
        _x > (_cur_eff / 2)
      }
    ] call INEPT_fnc_conditionalSelectIP;

    [_effects , {sqrt _x}] call INEPT_fnc_applyToAllIP;

    if (LOGGING_ENABLED()) then
    {
        LOGMSG("...");
        var(_cur_eff) = [_firepower, _cum_fp] call INEPT_fnc_getEffectiveness;
        LOGVAR(_cur_eff);
        {
            LOGVAR(_x);
        } forEach _selection;
        LOGVAR(_firepower);
        LOGVAR(_cum_fp);
        {
            LOGVAR(_some_items select _forEachIndex);
            LOGVAR(_x);
        } forEach _effects;
    };

    // Pick randomly weighted by effectiveness (instead of just the best) in an
    // effort to make things a little spicy.
    var(_idx) = [_effects] call cy_fnc_weightedRandom;
    if (_idx > -1) then
    {
        var(_best) = _some_items select _idx;
        LOGVAR(_best);
        _selection pushBack (_best select cy_data_DepotItem_source);
        _cum_fp = [_cum_fp, _best select cy_data_DepotItem_firepower] call INEPT_fnc_addFirepower;
    }
    else
    {
        inc(_failures);
    };

};

LOGMSG("Spawning ...");
LOGVAR(_firepower);
LOGVAR(_selection);

var(_grps) = [];

{
    _grps pushBack ([_depot, _area, _side, _x, _skill_profile, _key]
                    call cy_fnc_Depot_spawnUnits);
}
forEach _selection;

_grps
