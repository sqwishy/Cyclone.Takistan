#include "common\defines.h"

params ["_narrator_var", "_plot"];

private _narrator = missionNamespace getVariable _narrator_var;
[_narrator, _plot] call cy_fnc_Narrator_plotTerminated;
