#ifndef __PLATFORM_SQF__
#define __PLATFORM_SQF__

#include "common\defines.h"

// TODO, figure out a better way to do this
if (isNil "pl_data_new") then
{
    pl_data_new =
    {
        [0]
    };

    pl_data_attr =
    {
        var(_r) = argument select 0;
        argument set [0, _r + 1];
        _r
    };

    // This is a no-op, the intention is that you assign it to a variable that
    // is referenced whenever this data type is instantiated. Thereby leaving a
    // paper trail so you can easily find when you are trying to make a new
    // instance of the data type.
    // i.e. `["foo", 9001]` is harder to identify as an instantiation of a
    // datatype than `["foo", 9001] call my_data_Spam_init`. And the overhead
    // is negligible.
    pl_data_init =
    {
        argument
    };

    pl_data_null = []
};

#define DATA() \
    call pl_data_new;
#define ATTR(SRC) \
    SRC call pl_data_attr;

#endif
