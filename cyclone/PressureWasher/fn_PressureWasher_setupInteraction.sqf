#include "..\common\defines.h"

var(_vehicle) = argument_0;
var(_duration) = argument_1;

while {not ([_vehicle] call cy_fnc_PressureWasher_isDeactivated)} do
{
    waitUntil
    {
        sleep 0.5;
        [_vehicle] call cy_fnc_PressureWasher_canDeactivate
    };
    if ([ "Deactivating ..."
        , _duration
        , {[_vehicle] call cy_fnc_PressureWasher_canDeactivate}
        ] call BIS_fnc_keyHold) then
    {
        _vehicle setVariable [cy_var_PressureWasher_deactivated, true, true];
    };
};
