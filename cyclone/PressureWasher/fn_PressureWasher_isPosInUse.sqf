// Inquires as to if the given position is in use by the given pressure washer.

params ["_thrall", "_plot", "_pos"];

private _plotlet = [_thrall, _plot] call cy_fnc_Thrall_getPlotData;
if (isNil {_plotlet}) then
{
    //["The plotlet variable is unset on this machine - this function won't work as you expect"] call BIS_fnc_error;
    false
}
else
{
    _pos in (_plotlet select cy_data_PressureWasher_dangerzone)
}
