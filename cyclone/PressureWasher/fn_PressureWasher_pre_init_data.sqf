#include "..\platform.sqf"

cy_plot_PressureWasher =
    [ "cy_fnc_PressureWasher_server"
    , "cy_fnc_PressureWasher_player"
    , "cy_fnc_PressureWasher_ai" ];

cy_plot_PressureWasher set [cy_plot_fnc_isPosInUse, cy_fnc_PressureWasher_isPosInUse];
cy_plot_PressureWasher set [
    cy_plot_fnc_isAllowedQuickEntryAtPos,
    {not (_this call cy_fnc_PressureWasher_isPosInUse)}
    ];

cy_data_PressureWasher                   = DATA();
cy_data_PressureWasher_init              = pl_data_init;
cy_data_PressureWasher_side              = ATTR(cy_data_PressureWasher);
cy_data_PressureWasher_terminal          = ATTR(cy_data_PressureWasher);
cy_data_PressureWasher_cms               = ATTR(cy_data_PressureWasher);
cy_data_PressureWasher_desc_pos          = ATTR(cy_data_PressureWasher);
cy_data_PressureWasher_terminal_task_pos = ATTR(cy_data_PressureWasher);
cy_data_PressureWasher_cm_tasks_pos      = ATTR(cy_data_PressureWasher);
cy_data_PressureWasher_dangerzone_area   = ATTR(cy_data_PressureWasher);
cy_data_PressureWasher_dangerzone        = ATTR(cy_data_PressureWasher);

// Kept slightly brief to make network data silghtly smaller
cy_var_PressureWasher_deactivated = "cy_var_PW_deact";
