#define DEBUG_VAR "cy_fnc_PressureWasher_server_is_verbose"

#include "..\common\defines.h"

#define TERMINAL_CM_CLASS ["Land_TTowerSmall_1_F", "Land_TTowerSmall_2_F"]
#define TERMINAL_CLASS "Land_PressureWasher_01_F"

LOGENTRY();

params ["_thrall", "_plot", "_map", "_side", "_safe_zones"];

var(_hb) = [cy_var_plot_suggested_hb_lifetime] call cy_fnc_Heartbeat_init;
var(_gc_garbage) = [];
[_hb, _gc_garbage] spawn
{
    params ["_hb", "_gc_garbage"];
    waitUntil {sleep 1; ([_hb] call cy_fnc_Heartbeat_isDead)};
    LOGMSG("PressureWasher_narrator heartbeat dead");
    _gc_garbage spawn cy_fnc_GC;
};

// List of buildings we may wish to insert the pressure washer/node/terminal in
var(_samples) = [];
// I guess theoretically this could go on forever ... that is not good. The
// init shoud be separated from the narrator and this plot is added only if
// initilization is successful. See Issue #5.
while {count _samples < 4} do
{
    var(_buildings) = ([_map] call cy_fnc_Map_randomSpot) nearObjects ["House", 500];
    var(_nice_building_idx) =
        [ _buildings
        , {
            // the nearestBuilding check tries to ensure the building has a
            // path thingy.
            var(_spot) = getPosATL _x;
            ((nearestBuilding _x) isEqualTo _x)
            and {!((_x buildingPos 0) isEqualTo [0, 0, 0])}
            and {not ([_spot, _safe_zones] call cy_fnc_PressureWasher_isPosInSafeZone)}
            and {[[_thrall] call cy_fnc_Thrall_allPlots
                 ,{not ([_thrall, _x, cy_plot_fnc_isPosInUse, [_spot], false] call cy_fnc_Thrall_inquirePlot)}
                 ] call inept_fnc_all}
          }
        ] call INEPT_fnc_indexOfFirstAtRandom;
    if (_nice_building_idx != -1) then
    {
        _samples pushBack (_buildings select _nice_building_idx);
    };
};

var(_building_idx) =
    [ _samples
    , {
          // This whole thing is for checking surrounding area of our samples
          // to determine which area might a fun place to fight in.
          var(_building) = _x;
          var(_spot) = getPosATL _x;
          var(_spotarea) =
              [ 500
              , 500
              , 0
              , false
              , _spot
              ] call inept_data_Area_init;
          var(_subs) = [_spotarea, 50] call INEPT_fnc_subdivide;

          var(_buildingyness) = sqrt count (_spot nearObjects ["Building", 500]);
          var(_wateryness) = [];
          var(_availability) = [];
          {
              _availability set [ _forEachIndex
                                , if (([_spot, _safe_zones] call cy_fnc_PressureWasher_isPosInSafeZone) or
                                      {[[_thrall] call cy_fnc_Thrall_allPlots
                                       ,{[_thrall, _x, cy_plot_fnc_isPosInUse, [_spot], false] call cy_fnc_Thrall_inquirePlot}
                                       ] call inept_fnc_any })
                                      then { 0 } else { 1 }];
              _wateryness set [ _forEachIndex
                              , if (getTerrainHeightASL _x > 0)
                                    then { 0 } else { 1 }];
          }
          forEach _subs;

          (
            _buildingyness *
            (_availability call cy_fnc_averageNumber) *
            (1 - (abs (0.05 - (_wateryness call cy_fnc_averageNumber))) / 2)
          )
      }
    ] call INEPT_fnc_indexOfBest;

LOGVAR(_building_idx);
var(_building) = _samples select _building_idx;
var(_spot) = getPosATL _building;
LOGVAR(_building);
LOGVAR(_spot);

var(_terminal_pos) = (_building call BIS_fnc_buildingPositions) call BIS_fnc_selectRandom;
LOGVAR(_terminal_pos);
var(_min_dist) =
    if ([_terminal_pos] call INEPT_fnc_isPosUrban) then {
        50
    } else {
        350
    };

var(_cm_poss) = [];

// Find spots to put our scramblers
var(_i) = 1 + floor random 3;
while { _i > 0 } do
{
    _i = _i - 1;
    var(_ang) = random 360;
    var(_dist) = _min_dist + random 900;
    var(_offset) =
        [ [_dist, 0, 0]
        , _ang
        ] call INEPT_fnc_rotatedVector;
    var(_origin_pos) = _terminal_pos vectorAdd _offset;
    // TODO, don't use BIS_fnc_findSafePos, it will fuck you
    var(_pos) =
        [ _origin_pos
        // Min dist from origin
        , 0
        // Max dist from origin (roughly)
        , 200
        // Min dist from something else
        , 2
        // 0 - No water ...
        , 0
        // Max terrain gradient
        , 0.65
        // 0 - Shore not required
        , 0
        ] call BIS_fnc_findSafePos;
    if (!(_pos isEqualTo []) and {(_origin_pos distance _pos) < 300}) then {
        _cm_poss pushBack _pos;
    };
};

// Finally create the things

var(_terminal) = createVehicle
    [ TERMINAL_CLASS
    , _terminal_pos
    , []
    , 0
    , "NONE"
    ];
_terminal setVariable [cy_var_PressureWasher_deactivated, false];
_terminal setPosATL _terminal_pos;
_gc_garbage pushBack _terminal;

var(_reveal_offset) =
    if ([_terminal_pos] call INEPT_fnc_isPosUrban) then {
        random 20
    } else {
        random 60
    };
LOGVAR(_reveal_offset);
var(_reveal_pos) = [_terminal_pos + [], _reveal_offset] call INEPT_fnc_deviatedVector;
LOGVAR(_reveal_pos);

var(_active_cms_idx) = [];
var(_cms) = [];
var(_cms_task_pos) = [];
{
    var(_cm) = createVehicle
        [ TERMINAL_CM_CLASS call BIS_fnc_selectRandom
        , _x
        , []
        , 0
        , "NONE"
        ];
    _cm setVectorUp [random 0.02, random 0.02, 1];
    _cm setVariable [cy_var_PressureWasher_deactivated, false];
    _cms pushBack _cm;
    _gc_garbage pushBack _cm;
    _cms_task_pos pushBack (
        [getPosATL _cm, random 20] call INEPT_fnc_deviatedVector
    );
    _active_cms_idx pushBack _forEachIndex;
}
forEach _cm_poss;

var(_root_loc_desc_pos) =
    if (count _cms > 0) then {
        [[_cms, {getPosATL _x}] call INEPT_fnc_applyToAll] call cy_fnc_averagePosition
    } else {
        _terminal_pos
        vectorAdd
        ([[(random 12) ^ 2, 0, 0], random 360] call INEPT_fnc_rotatedVector)
    };

var(_dangerzone_area) =
    [ [ [[_terminal_pos, random 400] call INEPT_fnc_deviatedVector] + _cm_poss,
        { [500 + random 200, [_x select 0, _x select 1, 0]] call inept_data_Area_init_circle }
        ] call INEPT_fnc_applyToAll
    ] call cy_fnc_encompassingCircle;

// Collect our state and convey it to players

var(_plotlet) =
    [ _side              // cy_data_PressureWasher_side
    , _terminal          // cy_data_PressureWasher_terminal
    , _cms               // cy_data_PressureWasher_cms
    , _root_loc_desc_pos // cy_data_PressureWasher_desc_pos
    , _reveal_pos        // cy_data_PressureWasher_terminal_task_pos
    , _cms_task_pos      // cy_data_PressureWasher_cm_tasks_pos
    , _dangerzone_area   // cy_data_PressureWasher_dangerzone_area
    , []                 // cy_data_PressureWasher_dangerzone
    ];

LOGVAR(_plotlet);

[_thrall, _plot, _plotlet] call cy_fnc_Thrall_sendPlotData;

// Don't try sending this along with the plotlet, locations aren't serialized
// good apparently. But set it correctly on our local machine so that
// isPosInUse works.
var(_dangerzone) = [_dangerzone_area] call INEPT_fnc_locationFromArea;
_plotlet set [cy_data_PressureWasher_dangerzone, _dangerzone];

_gc_garbage pushBack _dangerzone;

var(_done) = false;

while
{
    sleep 1;
    (not _done)
    and {[_hb] call cy_fnc_Heartbeat_beat}
    and {not ([_plot] call cy_fnc_Plot_isCancelled)}
}
do
{
    if ([_terminal] call cy_fnc_PressureWasher_isDeactivated) then
    {
        _done = true;
    }
    else
    {
        var(_deactivated) =
            [ _active_cms_idx
            , {([_cms select _x] call cy_fnc_PressureWasher_isDeactivated)}
            ] call BIS_fnc_conditionalSelect;
        _active_cms_idx = _active_cms_idx - _deactivated;
    };
};

[_hb] call cy_fnc_Heartbeat_kill;
