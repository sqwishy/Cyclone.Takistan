#define DEBUG_VAR "cy_fnc_PressureWasher_ai_is_verbose"

#include "..\common\defines.h"

params ["_thrall", "_plot", "_depot", "_ai_skill_profile"];

private _plotlet  = [_thrall, _plot] call cy_fnc_Thrall_waitForPlotData;

var(_side)     = _plotlet select cy_data_PressureWasher_side;
var(_terminal) = _plotlet select cy_data_PressureWasher_terminal;
var(_cms)      = _plotlet select cy_data_PressureWasher_cms;

var(_active_cms_idx) = [];
{
    if (not ([_x] call cy_fnc_PressureWasher_isDeactivated)) then
    {
        _active_cms_idx pushBack _forEachIndex;
    };
}
forEach _cms;

// This could be improved by doing an urbanyness test of the surrounding area
// This really should be improved by basing the AoI size from the urbanyness
// and the amount of baddies from the AoI size.
var(_term_is_urban) = [getPosATL _terminal] call INEPT_fnc_isPosUrban;
var(_term_radius) = if (_term_is_urban) then { 150 } else { 350 };

var(_main_aois) = [];

_main_aois pushBack (
    [ [_term_radius, getPosATL _terminal] call inept_data_Area_init_circle
    , [6, 1, 1]
    , false // Is a reserve AoI?
    , 1     // BEHAVIOR_DEFEND (sorry for using the literal here)
    ] call INEPT_fnc_newAOI);

_main_aois pushBack (
    [ [800, getPosATL _terminal] call inept_data_Area_init_circle
    , [6 + random 4, 3, 6 + random 4]
    , true  // Is a reserve AoI?
    , 1     // BEHAVIOR_DEFEND (sorry for using the literal here)
    ] call INEPT_fnc_newAOI);

var(_cm_aois) =
    [ _active_cms_idx
    , {
          var(_cm) = _cms select _x;
          var(_cm_radius) = if ([getPosATL _cm] call INEPT_fnc_isPosUrban)
                            then {70 + random 300}
                            else {400 + random 800};
          [ [_cm_radius, getPosATL _cm] call inept_data_Area_init_circle
          , [8 + random 6, 5, 4]
          , false // Is a reserve AoI?
          , 1     // BEHAVIOR_DEFEND (sorry for using the literal here)
          ] call INEPT_fnc_newAOI
      }
    ] call INEPT_fnc_applyToAll;

var(_ao) = [_side] call INEPT_fnc_newAO;
{
    [_ao, _x] call INEPT_fnc_addAoI;
}
forEach (_main_aois + _cm_aois);

// Summon units!
private _grps = [];
{
    private _grp =
        [ _depot
        , [ _x select inept_data_AoI_dispersion
          , _x select inept_data_AoI_pos
          ] call inept_data_Area_init_circle
        , _side
        , _x select inept_data_AoI_firepower
        , _ai_skill_profile
        ] call cy_fnc_Depot_spawnForces;
    [_thrall, _plot, _grp] call cy_fnc_Thrall_addToPlotGC;
    _grps append _grp;
}
forEach (_main_aois + _cm_aois);

{
    [_ao, _x] call INEPT_fnc_addGroup;
}
forEach _grps;

// Garbage collection routine.
var(_hb) = [cy_var_plot_suggested_hb_lifetime] call cy_fnc_Heartbeat_init;
[_hb, _ao, _grps] spawn
{
    params ["_hb", "_ao", "_grps"];
    waitUntil {sleep 1; ([_hb] call cy_fnc_Heartbeat_isDead)};
    LOGMSG("PressureWasher_ai heartbeat dead");

    // Send units and their vehicles for deletion.
    {
        ([units _x, {vehicle _x}] call INEPT_fnc_applyToAll) call cy_fnc_GC;
        (units _x) call cy_fnc_GC;
    }
    forEach _grps;
};

while
{
    sleep cy_var_suggested_tick_interval;
    (alive _terminal)
    and {[_hb] call cy_fnc_Heartbeat_beat}
    and {not ([_plot] call cy_fnc_Plot_isCancelled)}
}
do
{
    // Trying to avoid stopping the loop in the event of catastrophic failure.
    // Is this really the best way to do that?
    var(_handle) = [_ao] spawn INEPT_fnc_processAOOnce;
    waitUntil {sleep 0.05; scriptDone _handle};

    [_hb] call cy_fnc_Heartbeat_beat;

    [ _active_cms_idx
    , {
        var(_is_inactive) = [_cms select _x] call cy_fnc_PressureWasher_isDeactivated;
        if (_is_inactive) then
        {
            var(_aoi) = _cm_aois select _forEachIndex;
            [_ao, _aoi] call INEPT_fnc_removeAOI;
            // Tag elements we want to remove with -1 and remove them later as
            // to not alter the array while we iterate.
            // TODO, use better data structures
            _cm_aois set [_forEachIndex, -1];
            -1
        }
        else
        {
            _x
        }
      }
    ] call INEPT_fnc_applyToAllIP;
    _active_cms_idx = _active_cms_idx - [-1];
    _cm_aois = _cm_aois - [-1];

    [_hb] call cy_fnc_Heartbeat_beat;
};

[_hb] call cy_fnc_Heartbeat_kill;
