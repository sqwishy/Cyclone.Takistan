#include "common\defines.h"

params ["_narrator", "_plot"];

// TODO, move into cy_fnc_Plot_cancel or something
_plot set [cy_data_Plot_hc_slot, objNull];

private _cyclone = _narrator select cy_data_Narrator_cyclone;
private _jipid = [_cyclone, "jip", _plot] call cy_fnc_Cyclone_plotVar;
// Inform the audiance of our change to the plot
LOGMSG(_fnc_scriptName);
LOGVAR(_jipid);
LOGMSG("cy_fnc_Audiance_updatePlotByRemote");
LOGVAR(_plot);
[_narrator select cy_data_Narrator_audiance_var, _plot]
	remoteExec ["cy_fnc_Audiance_updatePlotByRemote", REMOTE_EXEC_EVERYWHERE, _jipid];

[_narrator, _plot] call cy_fnc_Narrator_plotTerminated;
