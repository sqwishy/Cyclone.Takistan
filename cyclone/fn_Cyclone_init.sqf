// Arguments
//   _server_logic:
//      A vehicle (such as a logic object) guarantted to always be local to the
//      server.
//  _hc_slots:
//      A list of vehicle names for HC slots.
//  _remote_exec_id:
//      A string used to keep track of the remoteExec JIP ID. Should be the
//      same for cyclone instances that want their thralls to be in sync.

#include "common\defines.h"

params ["_server_logic",
        ["_hc_slots", []],
        ["_remote_exec_id", "cy_plots"]];

var(_cyclone) =
    [ _server_logic   // cy_data_Cyclone_server_logic
    , _hc_slots       // cy_data_Cyclone_hc_slots
    , _remote_exec_id // cy_data_Cyclone_remote_exec_id
    , []              // cy_data_Cyclone_drivers
    , "@cy"           // cy_data_Cyclone_magic_prefix
    , isServer        // cy_data_Cyclone_is_narrator
    , hasInterface    // cy_data_Cyclone_is_player
    , false           // cy_data_Cyclone_is_AI
    ] call cy_data_Cyclone_init;

_cyclone set [
    cy_data_Cyclone_is_AI,
    if (isMultiplayer and cy_param_WithHeadlessClients) then
    {
        if (isServer) then
        {
            false
        }
        else
        {
            waitUntil {not isNull player};
            [ [_cyclone] call cy_fnc_Cyclone_getHCs
            , {_x isEqualTo player}
            ] call INEPT_fnc_any
        }
    }
    else
    {
        isServer
    }
];

_cyclone
