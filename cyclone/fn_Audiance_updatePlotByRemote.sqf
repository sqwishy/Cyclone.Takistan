#include "common\defines.h"

params ["_audiance_var", "_plot"];

LOGENTRY();

// This is probably needed for JIP players. The remoteExec that invokes this is
// persistent, but it may be called before the audiance has been set up.
waitUntil {sleep 0.1; not isNil _audiance_var};

private _audiance = missionNamespace getVariable _audiance_var;
private _plot_data_var = [_audiance, _plot] call cy_fnc_Audiance_plotDataVar;

MUTEX_LOCK(_audiance select cy_data_Audiance_mutex);

if (isNil _plot_data_var) then
{
	missionNamespace setVariable [_plot_data_var, _plot];
	(_audiance select cy_data_Audiance_last_value) pushBack _plot_data_var;
	// Either the plot is new or we're new, either way the plot isn't running
	// so we shall invoke it.
	{
		private _thrall = _x;
		if (not (_thrall select cy_data_Thrall_check_hc_slot)
			or {local (_plot select cy_data_Plot_hc_slot)}) then
		{
			[_thrall, _plot] spawn cy_fnc_Thrall_invokePlot;
		};
	}
	forEach (_audiance select cy_data_Audiance_members);
}
else
{
	[missionNamespace getVariable _plot_data_var, _plot] call cy_fnc_copyInto;
};

MUTEX_UNLOCK(_audiance select cy_data_Audiance_mutex);
