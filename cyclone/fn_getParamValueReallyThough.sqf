// Like BIS_fnc_getParamValue but, as long as it is run in scheduled execution
// environment, it will wait until it actually has the mission parameter value
// before returning instead of silently failing like an asshole.

private ["_value"];
waitUntil
{
    _value = [_this] call BIS_fnc_getParamValue;
    not isNil "_value"
};
_value
