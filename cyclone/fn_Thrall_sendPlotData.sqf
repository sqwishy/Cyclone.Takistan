params ["_thrall", "_plot", "_data", "_key"];

private _var = [_thrall, _plot select cy_data_Plot_id] call cy_fnc_Thrall_plotletVar;
if !(isNil "_key") then
{
    _var = [_var, _key] joinString "_";
};

missionNamespace setVariable [_var, _data, true];
