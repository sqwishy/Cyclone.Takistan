params ["_dst", "_src", ["_recursive", true]];
assert ((count _dst) isEqualTo (count _src));
{
	private _dst_x = _dst select _forEachIndex;
	if (_x isEqualType []) then
	{
		[_dst_x, _x] call cy_fnc_copyInto;
	}
	else
	{
		_dst set [_forEachIndex, _x];
	};
}
forEach _src;
