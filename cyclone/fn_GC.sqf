/* cy_fnc_GC
 *
 * Like BIS_fnc_GC but completely different and works in multiplayer by taking
 * into consideration all players.
 */

if (isNil "cy_var_GC_queue") then
{
    cy_var_GC_queue = [];
    [cy_var_GC_queue] spawn cy_fnc_GC_init;
};
cy_var_GC_queue append _this;
