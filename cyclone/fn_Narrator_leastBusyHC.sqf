// Returns:
//  A pair [Object: headless client object, Number: unit count] for the least
//  busy AI hosting machine. 
//  The server logic object as the first parameter if in single player or when
//  headless clients are disabled.
//  Or returns objNull as the first parameter when using headless clients and
//  they have all fallen over and disconnected.

#include "common\defines.h"

params ["_narrator"];

var(_hcs) = nil;
if (isMultiplayer and cy_param_WithHeadlessClients) then
{
    _hcs = [_narrator] call cy_fnc_Narrator_activeHCs;
}
else
{
    _hcs = [_narrator select cy_data_Narrator_cyclone select cy_data_Cyclone_server_logic];
};

LOGMSG("Active AI controllers");
LOGVAR(_hcs);

// TODO, consider doing something involving who has claimed units, not just
// their locality.
var(_units) = allUnits - playableUnits - switchableUnits;
var(_usage) =
    [ _hcs
    , {var(_owner) = owner _x; {_owner isEqualTo owner _x} count _units}
    ] call INEPT_fnc_applyToAll;

var(_idx) = [_usage, {-_x}] call INEPT_fnc_indexOfBest;
if (_idx isEqualTo -1) then
{
    [objNull, -1]
}
else
{
    [_hcs select _idx, _usage select _idx]
}
