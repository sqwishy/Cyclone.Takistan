// Arguments
//  _cyclone:
//  _plot_role:
//      Something of cy_var_PlotRole_*
//  _narrator_var:
//      Variable name of narrator object on the server's missionNamespace

#include "common\defines.h"

params ["_cyclone", "_plot_role", ["_narrator_var", ""]]; 

[ _cyclone                                // cy_data_Thrall_cyclone
, _plot_role                              // cy_data_Thrall_plot_role
, _plot_role isEqualTo cy_var_PlotRole_ai // cy_data_Thrall_check_hc_slot
, []                                      // cy_data_Thrall_plots
, NEW_MUTEX()                             // cy_data_Thrall_plots_mutex
, _narrator_var                           // cy_data_Thrall_narrator_var
] call cy_data_Thrall_init
