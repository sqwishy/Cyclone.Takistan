#include "common\defines.h"

var(_weights) = argument_0;

if (count _weights < 1) exitWith { -1 };

var(_sum) = 0;
{
    _sum = _sum + _x;
}
forEach _weights;

var(_threshold) = random _sum;
var(_cursor) = 0;

var(_idx) =
    {
        _cursor = _cursor + _x;
        if (_cursor >= _threshold) exitWith
        {
            _forEachIndex
        };
        // I think the only case we would get here at the end of the loop is if
        // there is an issue with floating point precision. In that case, just
        // return the index of the last element as that probably was was
        // selected.
        (count _weights - 1)
    }
    forEach _weights;

_idx
