// This should be invoked by a spawn or something. It might not return for a long time.
//
// Arguments
//   _thrall:
//   _plot:

#include "common\defines.h"

LOGENTRY();

params ["_thrall", "_plot"];

LOGVAR(_plot);

private _driver =
    _thrall select cy_data_Thrall_cyclone
            select cy_data_Cyclone_drivers
            select (_plot select cy_data_Plot_driver_serial);

private _callme_name =
    (_driver select cy_data_PlotDriver_mapping) select (_thrall select cy_data_Thrall_plot_role);

private _args = [_thrall, _plot];

switch (_thrall select cy_data_Thrall_plot_role) do
{
case cy_var_PlotRole_server:
{ _args append (_driver select cy_data_PlotDriver_server_args) };
case cy_var_PlotRole_player:
{ _args append (_driver select cy_data_PlotDriver_player_args) };
case cy_var_PlotRole_ai:
{ _args append (_driver select cy_data_PlotDriver_ai_args) };
};

LOGMSG("Invoking...");
LOGVAR(_callme_name);

MUTEX_LOCK(_thrall select cy_data_Thrall_plots_mutex);
(_thrall select cy_data_Thrall_plots) pushBack _plot;
MUTEX_UNLOCK(_thrall select cy_data_Thrall_plots_mutex);

_args call (missionnamespace getVariable _callme_name);

MUTEX_LOCK(_thrall select cy_data_Thrall_plots_mutex);
[ _thrall select cy_data_Thrall_plots
, {_x select cy_data_Plot_id != _plot select cy_data_Plot_id}
] call inept_fnc_conditionalSelectIP;
MUTEX_UNLOCK(_thrall select cy_data_Thrall_plots_mutex);

LOGMSG("Exited ...");
LOGVAR(_callme_name);

if ((_thrall select cy_data_Thrall_plot_role) isEqualTo cy_var_PlotRole_server) then
{
    LOGMSG(_fnc_scriptName);
    LOGMSG("cy_fnc_Narrator_plotTerminatedByRemote");
    [_thrall select cy_data_Thrall_narrator_var, _plot]
        remoteExec ["cy_fnc_Narrator_plotTerminatedByRemote", SERVER_CID];
};
