/* This is unrelated to Cyclone. It's here just because ...
 *
 * An explanation:
 * 
 *         -> Record (-->) Pattern
 *        /
 *  Binding
 *        \
 *         -> VTable (-->) Interface
 *
 *  (-->) Indicates an "occurance of" relationship.
 *
 *  Bindings are passed to functions that are designed to use an interface.
 */

ox_const_MemberAttribute = 0;
ox_const_MemberMethod    = 1;

#define ox_Binding_record 0
#define ox_Binding_vtable 1

ox_fnc_Pattern =
{
    []
};

ox_fnc_Attr =
{
    // The default thing here doesn't work and never may
    params ["_pat", "_default"];
    assert (not isNil "_pat");
    _pat pushBack [ox_const_MemberAttribute, 1];
    (count _pat - 1)
};

ox_fnc_Meth =
{
    params ["_pat"];
    assert (not isNil "_pat");
    _pat pushBack [ox_const_MemberMethod];
    (count _pat - 1)
};

ox_fnc_Record =
{
    params ["_pat", "_args"];
    assert ((count _pat) isEqualTo (count _args));
    _args
};

ox_fnc_Interface =
{
    []
};

ox_fnc_IAttr =
{
    _this call ox_fnc_Attr
};

// Takes [_vtable_index, [_binding, ...]]
// _vtable_index is the index of a method in a vtable.
// Invokes the callable in the binding's vtable at the index passing the
// binding's record as well the extra ... arguments.
ox_fnc_IMeth_invoke =
{
    params ["_index", "_args"];
    private _binding = _args select 0;
    _args = _args + [];
    _args set [0, _binding select ox_Binding_record];
    _args call ((_binding select ox_Binding_vtable) select _index)
};

// Similar to ox_fnc_IAttr, but instead of returning the index, returns a
// code block that invokes ox_fnc_IMeth_invoke with the index.
ox_fnc_IMeth =
{
    compile format ["[%1, _this] call ox_fnc_IMeth_invoke", _this call ox_fnc_Attr]
};

ox_fnc_Get =
{
    params ["_binding", "_idx"];
    (_binding select ox_Binding_record) select ((_binding select ox_Binding_vtable) select _idx)
};

ox_fnc_Set =
{
    params ["_binding", "_idx", "_val"];
    (_binding select ox_Binding_record) select ((_binding select ox_Binding_vtable) set [_idx, _val])
};

ox_fnc_Bind =
{
    _this
};

// Declare Cow

test_Cow          = [] call ox_fnc_Pattern;
test_Cow_name     = [test_Cow] call ox_fnc_Attr;
test_Cow_num_legs = [test_Cow] call ox_fnc_Attr;

test_fnc_Cow_init =
{
    [test_Cow, _this] call ox_fnc_Record
};

test_fnc_Cow_speak =
{
    params ["_cow"];
    diag_log format ["Moo, my name is %1", _cow select test_Cow_name];
};

test_fnc_Cow_feed =
{
    params ["_cow", "_food"];
    if (_food == "chocolate") then
    {
        diag_log "Chocolate! Yay!";
    }
    else
    {
        diag_log format ["Cows only eat chocolate, not %1.", _food];
    };
};

// Declare IAnimal

test_IAnimal          = [] call ox_fnc_Interface;
test_IAnimal_num_legs = [test_IAnimal] call ox_fnc_IAttr;
test_IAnimal_speak    = [test_IAnimal] call ox_fnc_IMeth;
test_IAnimal_feed     = [test_IAnimal] call ox_fnc_IMeth;

//// Example usage of the above API

test_Cow_implements_test_IAnimal =
    [ test_Cow_num_legs
    , test_fnc_Cow_speak
    , test_fnc_Cow_feed ];

// Regular usage

daisy = ["Daisy", 4] call test_fnc_Cow_init;

diag_log "... invoking like normal";

diag_log format ["This animal has %1 legs.", daisy select test_Cow_num_legs];
[daisy] call test_fnc_Cow_speak;
[daisy, "apple"] call test_fnc_Cow_feed;
[daisy, "chocolate"] call test_fnc_Cow_feed;

// Polymorphism

daisy_as_IAnimal = [daisy, test_Cow_implements_test_IAnimal] call ox_fnc_Bind;

diag_log "... invoking via binding";

barn = [daisy_as_IAnimal];

{
    private _animal = _x;
    diag_log format ["This animal has %1 legs.", [_animal, test_IAnimal_num_legs] call ox_fnc_Get];
    [_animal] call test_IAnimal_speak;
    [_animal, "rocky road ice cream"] call test_IAnimal_feed;
    [_animal, "chocolate"] call test_IAnimal_feed;
}
forEach barn;

diag_log "fin";
