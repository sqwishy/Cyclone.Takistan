#include "..\ineptifex\data.h"

INEPT_fnc_getFirepower_is_verbose = true;
zz = [];
zz pushBack (", , I, AI, R, AR, A, AA, move, camo");
{
    zz pushBack (format [ "%1, %2, %3"
                        , getText (configFile >> "CfgVehicles" >> typeOf _x >> "displayName")
                        , typeOf _x
                        , [[_x, UNIT_INFO_ACCURATE, UNIT_INFO_WITH_CARGO] call INEPT_fnc_getUnitInfo] call INEPT_fnc_getFirepower
                        ]);
}
forEach _this;
INEPT_fnc_getFirepower_is_verbose = nil;
zz pushBack ("
");
copyToClipboard (zz joinString"
");
